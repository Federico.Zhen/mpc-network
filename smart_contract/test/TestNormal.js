const ArpaMpc = artifacts.require("./ArpaMpc.sol");

contract('Test ArpaMpc normal process', function (accounts) {

    //accounts
    // accounts[0] must not be a data provider, a worker or a customer
    const [
        coordinator,
        dataProviderWorker,
        additionalWorker1,
        additionalWorker2,
        customer,
        delegatedWorker
    ] = accounts

    const testProgramHash = "Test program hash";

    const stages = {
        Initialized: 0,
        Running: 1,
        Done: 2
    }

    let workerList = [dataProviderWorker, additionalWorker1, additionalWorker2, delegatedWorker];
    let dataProviderWorkers = [dataProviderWorker];
    let feeForDataProviderWorkers = [web3.utils.toWei('2', 'ether')]

    let mpcRequest = {
        numWorkers: 2,
        programHash: testProgramHash,
        dataProviderWorkers: dataProviderWorkers,
        delegatedWorker: delegatedWorker,
        feeForDataProviderWorkers: feeForDataProviderWorkers,
        feePerAdditionalWorker: web3.utils.toWei('1', 'ether'),
    }

    // TODO(John)? adjust automatically
    let feeToPay = web3.utils.toWei('5', 'ether')
    let result = "Test String Result";
    
    let requestId = 0; // should be 0 for the first request 

    let arpaMpcInstance;

    before(async () => {
        arpaMpcInstance = await ArpaMpc.new(coordinator, {from: coordinator});
        console.log("Contract instance obtained")
    });

    it('should update worker list', async () => {

        let receipt = await arpaMpcInstance.updateWorkerList(workerList, [], {from: coordinator});
        let workerListFromEvent = receipt.logs[0].args._workerList;
        assert.equal(web3.utils.sha3(workerList), web3.utils.sha3(workerListFromEvent), "worker list from event is different");
    });

    it('should send an MPC request', async () => {
        
        let receipt = await arpaMpcInstance.requestMpc(
            mpcRequest.numWorkers,
            mpcRequest.programHash,
            mpcRequest.dataProviderWorkers,
            mpcRequest.delegatedWorker,
            mpcRequest.feeForDataProviderWorkers,
            mpcRequest.feePerAdditionalWorker, 
            {from: customer, value: feeToPay});

        assert.equal(receipt.logs[0].args._reqId, 0, "request Id is wrong");
        assert.equal(receipt.logs[0].args._numWorkers, mpcRequest.numWorkers, "number of worker is wrong");
        assert.equal(receipt.logs[0].args._programHash.replace(/\0/g, ''), "Test program hash", "program hash is wrong");
        assert.equal(web3.utils.sha3(receipt.logs[0].args._dataProviderWorkers), web3.utils.sha3(mpcRequest.dataProviderWorkers), "data provider workers are wrong");
        assert.equal(receipt.logs[0].args._dataConsumer, customer, "data consumer is wrong");
        assert.equal(receipt.logs[0].args._delegatedWorker, mpcRequest.delegatedWorker, "delegated worker is wrong");
        assert.equal(web3.utils.sha3(receipt.logs[0].args._feeForDataProviderWorkers), web3.utils.sha3(mpcRequest.feeForDataProviderWorkers), "fee for data provider workers is wrong");
        assert.equal(receipt.logs[0].args._feePerAdditionalWorker, mpcRequest.feePerAdditionalWorker, "fee for additional worker is wrong");
        
        let stage = (await arpaMpcInstance.requests(requestId)).currentStage;
        assert.equal(stage, stages.Initialized, "wrong current stage");
    
    });

    it('should tell the blockchain computation has started', async () => {
        
        let receipt = await arpaMpcInstance.startComputation(
            requestId,
            [additionalWorker1, additionalWorker2], 
            {from: coordinator});
        assert.equal(receipt.logs[0].args._reqId, 0, "request Id is wrong");

        let stage = (await arpaMpcInstance.requests(requestId)).currentStage;
        assert.equal(stage, stages.Running, "wrong current stage");
    });

    it('should post the result and distribute awards', async () => {
        
        let receipt = await arpaMpcInstance.postResult(
            requestId,
            result,
            {from: coordinator});
        assert.equal(receipt.logs[0].args._reqId, requestId, "request Id is wrong");
        assert.equal(receipt.logs[0].args._result, result, "result is wrong");

        let stage = (await arpaMpcInstance.requests(requestId)).currentStage;
        assert.equal(stage, stages.Done, "wrong current stage");
    });

    it('should withdraw awards', async () => {
        await arpaMpcInstance.withdraw({from: dataProviderWorker});
        await arpaMpcInstance.withdraw({from: additionalWorker1});
        await arpaMpcInstance.withdraw({from: additionalWorker2});
    });
    
});
