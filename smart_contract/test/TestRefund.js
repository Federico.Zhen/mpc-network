const ArpaMpc = artifacts.require("./ArpaMpc.sol");
const { timeTravel } = require("./helper/timeTravel");

contract('Test ArpaMpc refund process', function (accounts) {

    const expireTime = 7*60*60 // 7 hours

    //accounts
    // accounts[0] must not be a data provider, a worker or a customer
    const [
        coordinator,
        dataProviderWorker,
        additionalWorker1,
        additionalWorker2,
        customer,
        delegatedWorker
    ] = accounts

    const testProgramHash = "Test program hash";
    const maxWorkerNum = 10;

    const stages = {
        Initialized: 0,
        Running: 1,
        Done: 2,
        Failed: 3
    }

    let workerList = [dataProviderWorker, additionalWorker1, additionalWorker2, delegatedWorker];
    let dataProviderWorkers = [dataProviderWorker];
    let feeForDataProviderWorkers = [web3.utils.toWei('2', 'ether')]

    let mpcRequest = {
        numWorkers: 2,
        programHash: testProgramHash,
        dataProviderWorkers: dataProviderWorkers,
        delegatedWorker: delegatedWorker,
        feeForDataProviderWorkers: feeForDataProviderWorkers,
        feePerAdditionalWorker: web3.utils.toWei('1', 'ether'),
    }

    // TODO(John)? adjust automatically
    let feeToPay = web3.utils.toWei('5', 'ether')
    let result = "Test String Result";
    
    let requestId = 0; // should be 0 for the first request 

    let arpaMpcInstance;

    before(async () => {
        arpaMpcInstance = await ArpaMpc.new(coordinator, {from: coordinator});
        console.log("Contract instance obtained")
    });

    it('should update worker list', async () => {

        let receipt = await arpaMpcInstance.updateWorkerList(workerList, [], {from: coordinator});
        let workerListFromEvent = receipt.logs[0].args._workerList;
        assert.equal(web3.utils.sha3(workerList), web3.utils.sha3(workerListFromEvent), "worker list from event is different");
    });

    it('should not allow an MPC request that exceeds max worker number', async () => {
        
        let hasThrown = false;
        try {
            await await arpaMpcInstance.requestMpc(
                maxWorkerNum + 1,
                mpcRequest.programHash,
                mpcRequest.dataProviderWorkers,
                mpcRequest.delegatedWorker,
                mpcRequest.feeForDataProviderWorkers,
                mpcRequest.feePerAdditionalWorker, 
                {from: customer, value: feeToPay}
            );
        } catch(err) {
            assert.include(err.toString(), "revert", "an MPC request that exceeds max worker number did not trigger a revert'");
            hasThrown = true;
        }
        assert.isTrue(hasThrown, "an MPC request that exceeds max worker number is allowed");
    
    });

    it('should send an MPC request', async () => {
        
        let receipt = await arpaMpcInstance.requestMpc(
            mpcRequest.numWorkers,
            mpcRequest.programHash,
            mpcRequest.dataProviderWorkers,
            mpcRequest.delegatedWorker,
            mpcRequest.feeForDataProviderWorkers,
            mpcRequest.feePerAdditionalWorker, 
            {from: customer, value: feeToPay});

        assert.equal(receipt.logs[0].args._reqId, 0, "request Id is wrong");
        assert.equal(receipt.logs[0].args._numWorkers, mpcRequest.numWorkers, "number of worker is wrong");
        assert.equal(receipt.logs[0].args._programHash, "Test program hash", "program hash is wrong");
        assert.equal(web3.utils.sha3(receipt.logs[0].args._dataProviderWorkers), web3.utils.sha3(mpcRequest.dataProviderWorkers), "data provider workers are wrong");
        assert.equal(receipt.logs[0].args._dataConsumer, customer, "data consumer is wrong");
        assert.equal(receipt.logs[0].args._delegatedWorker, mpcRequest.delegatedWorker, "delegated worker is wrong");
        assert.equal(web3.utils.sha3(receipt.logs[0].args._feeForDataProviderWorkers), web3.utils.sha3(mpcRequest.feeForDataProviderWorkers), "fee for data provider workers is wrong");
        assert.equal(receipt.logs[0].args._feePerAdditionalWorker, mpcRequest.feePerAdditionalWorker, "fee for additional worker is wrong");
        
        let stage = (await arpaMpcInstance.requests(requestId)).currentStage;
        assert.equal(stage, stages.Initialized, "wrong current stage");
    
    });

    it('should not be able to refund before expiration', async () => {
        
        let hasThrown = false;
        try {
            await arpaMpcInstance.refund(
                requestId,
                {from: customer}
            );
        } catch(err) {
            assert.include(err.toString(), "revert", "refund before expiration did not trigger a revert'");
            hasThrown = true;
        }
        assert.isTrue(hasThrown, "can refund before expiration");

    });

    it('should refund after expiration', async () => {
        
        let initBalance = await web3.eth.getBalance(customer);
        await timeTravel(expireTime);
        await arpaMpcInstance.refund(
            requestId,
            {from: customer}
        );
        let stage = (await arpaMpcInstance.requests(requestId)).currentStage;
        assert.equal(stage, stages.Failed, "wrong current stage");
        let finalBalance = await web3.eth.getBalance(customer);
        assert.isAbove(parseInt(finalBalance), parseInt(initBalance), "user did not receive refund")

    });
    
});
