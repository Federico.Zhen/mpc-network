const ArpaMpc = artifacts.require("./ArpaMpc.sol");

contract('Test ArpaMpc Update Worker List', function (accounts) {

    //accounts
    const [
        coordinator,
        w1,
        w2,
        w3,
        w4,
        w5
    ] = accounts

    let tests = [
        {
            add: [w1, w2, w3, w4, w5],
            remove: [],
            result: [w1, w2, w3, w4, w5],
        },
        {
            add: [],
            remove: [w5],
            result: [w1, w2, w3, w4],
        },
        {
            add: [],
            remove: [w2],
            result: [w1, w4, w3],
        },
        {
            add: [w5],
            remove: [w1, w4],
            result: [w5, w3],
        }
    ]

    let arpaMpcInstance;

    before(async () => {
        arpaMpcInstance = await ArpaMpc.new(coordinator, {from: coordinator});
        console.log("Contract instance obtained")
    });

    it('should update worker list', async () => {

        for (let i = 0; i < tests.length; i++) {
            console.log("Update worker list for test case " + i.toPrecision())
            let receipt = await arpaMpcInstance.updateWorkerList(tests[i].add, tests[i].remove, {from: coordinator});
            let workerListFromEvent = receipt.logs[0].args._workerList;
            assert.equal(web3.utils.sha3(tests[i].result), web3.utils.sha3(workerListFromEvent), "worker list from event is different for test case " + i.toPrecision());
        }
    });

});