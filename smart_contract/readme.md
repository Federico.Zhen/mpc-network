## ArpaMpc
Ethereum smart contract for ARPA testnet 1.0
## Get Started
Under this directory (Same for the rest commands)
```
npm install
```
## Compile
Compile all smart contracts
```
npm run compile
```
## Test
Run all test 
```
npm test
```
