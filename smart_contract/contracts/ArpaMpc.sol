pragma solidity ^0.5.0;

import "../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";

contract ArpaMpc {
    using SafeMath for uint;

    address constant ADDRESS_NOT_ASSIGNED = address(0x0);
    uint constant REQUEST_EXPIRE_TIME = 6 hours;
    uint constant MAX_WORKER_NUM = 10;

    // stage of an MPC request
    enum Stage {
        Initialized,
        Running,
        Done,
        Failed
    }
    
    struct Request {
        uint id;
        uint numWorkers;
        string programHash;
        address payable dataConsumer;
        address[] dataProviderWorkers;
        address[] additionalWorkers;
        address delegatedWorker; // If equals ADDRESS_NOT_ASSIGNED, means not assigned
        uint[] feeForDataProviderWorkers;
        uint feePerAdditionalWorker;
        uint totalFee;
        uint receivedTime;
        Stage currentStage;
    }
    
    address[] public workerList; // a list of currently registered workers
    mapping(address=>bool) isWorker;
    mapping(address=>uint) workerIndex; // record worker's index in workerList, so that remove costs o(1)
    mapping(address=>uint) vault; // place to store workers rewards
    Request[] public requests; 
    address public coordinator;
    uint public lastReqId = 0;

    modifier onlyCoordinator {
        require(msg.sender == coordinator, "sender must be the coordinator");
        _;
    }

    modifier atStage(uint _reqId, Stage _stage) {
        require(_stage == requests[_reqId].currentStage, "please follow the procedure");
        _;
    }

    constructor(address _coordinator) public {
        coordinator = _coordinator;
    }
    
    /**
     * @dev coordinator update a new set of valid workers
     * @param _workersToAdd workers to be added to the list
     * @param _workersToRemove workers to be removed from the list
     */
    function updateWorkerList(address[] calldata _workersToAdd, address[] calldata _workersToRemove) external onlyCoordinator {
        for (uint i = 0; i < _workersToAdd.length; i++) {
            if (!isWorker[_workersToAdd[i]]) {
                isWorker[_workersToAdd[i]] = true;
                workerIndex[_workersToAdd[i]] = workerList.length;
                workerList.push(_workersToAdd[i]);
            }
        }
        for (uint i = 0; i < _workersToRemove.length; i++) {
            if (isWorker[_workersToRemove[i]]) {
                // remove its worker flag
                delete isWorker[_workersToRemove[i]];
                // remove it from the workerList
                // (by swapping with the last worker)
                address workerToSwap = workerList[workerList.length - 1];                
                workerList[workerIndex[_workersToRemove[i]]] = workerToSwap;
                delete workerList[workerList.length - 1];
                workerList.length--;
                // remove its corresponding workerIndex
                // (and adjust the swapped worker's index)
                workerIndex[workerToSwap] = workerIndex[_workersToRemove[i]];
                delete workerIndex[_workersToRemove[i]];
            }
        }
        emit WorkerListUpdated(workerList);
    } 
    
    /**
     * @dev data consumer call this to initalize a new MPC request
     * @dev with enough payment msg.value
     */
    function requestMpc(
        uint _numWorkers,
        string  calldata _programHash,
        address[] calldata _dataProviderWorkers,
        address _delegatedWorker,
        uint[] calldata _feeForDataProviderWorkers,
        uint _feePerAdditionalWorker
        ) external payable {
        require(_numWorkers <= MAX_WORKER_NUM, "number of workers exceeds allowance");
        require(_dataProviderWorkers.length == _feeForDataProviderWorkers.length, "number of data provider worker wrong");
        if (_delegatedWorker != ADDRESS_NOT_ASSIGNED) {
            require(_dataProviderWorkers.length <= _numWorkers.sub(1), "more workers than specified");
            require(isWorker[_delegatedWorker], "delegated worker must be a worker");
        } else {
            require(_dataProviderWorkers.length <= _numWorkers, "more workers than specified");
        }
        uint totalFee;
        uint numNonDataProviderWorkers = _numWorkers.sub(_dataProviderWorkers.length); // number of workers that are not data provider workers
        totalFee = numNonDataProviderWorkers.mul(_feePerAdditionalWorker);
        for (uint i = 0; i < _dataProviderWorkers.length; i++) {
            require(isWorker[_dataProviderWorkers[i]], "data provider worker must be a worker");
            require(_dataProviderWorkers[i] != _delegatedWorker, "delegated worker is already a data provider worker");
            totalFee = totalFee.add(_feeForDataProviderWorkers[i]);
        }
        require(msg.value >= totalFee, "insufficient fund");

        uint currReqId = lastReqId;
        Request memory newRequest = Request(
            currReqId,
            _numWorkers,
            _programHash,
            msg.sender,
            _dataProviderWorkers,
            new address[](0),
            _delegatedWorker,
            _feeForDataProviderWorkers,
            _feePerAdditionalWorker,
            msg.value,
            block.timestamp,
            Stage.Initialized);
        requests.push(newRequest);
        emit RequestReceived(
            currReqId,
            _numWorkers,
            _programHash,
            msg.sender,
            _dataProviderWorkers,
            _delegatedWorker,
            _feeForDataProviderWorkers,
            _feePerAdditionalWorker);
        lastReqId++;
    }
    
    /**
     * @dev having heard a MPC request, the coordinator selects a worker group to start the computation
     * @dev and call this function to update the status of the request 
     */
    function startComputation(
        uint _reqId, 
        address[] calldata _additionalWorkers
        ) external 
        onlyCoordinator 
        atStage(_reqId, Stage.Initialized) {
        Request memory request = requests[_reqId];
        uint numAdditionalWorkers;
        if (request.delegatedWorker != ADDRESS_NOT_ASSIGNED) {
            numAdditionalWorkers = request.numWorkers.sub(request.dataProviderWorkers.length).sub(1);  
        } else {
            numAdditionalWorkers = request.numWorkers.sub(request.dataProviderWorkers.length); 
        }
        if (numAdditionalWorkers > 0) {
            require(_additionalWorkers.length == numAdditionalWorkers, "wrong number of additional workers");
            for (uint i = 0; i < _additionalWorkers.length; i++) {
                require(isWorker[_additionalWorkers[i]], "all computation parties must become workers first");
            }
            requests[_reqId].additionalWorkers = _additionalWorkers;
        }
        requests[_reqId].currentStage = Stage.Running;
        emit ComputationStarted(_reqId);
    }
    
    /**
     * @dev after the computation, the coordinator post the results and the contract will distribute
     * @dev rewards to each party
     */
    function postResult(uint _reqId, string calldata _result) external onlyCoordinator atStage(_reqId, Stage.Running) {
        requests[_reqId].currentStage = Stage.Done;
        distributeRewards(_reqId);
        emit ResultPosted(_reqId, _result);
    }

    /**
     * @dev when there is something wrong accidentally that the calculation can not 
     * @dev be started, data customer can ask for a refund (anyone can do that on behalf of him)
     */
    function refund(uint _reqId) external atStage(_reqId, Stage.Initialized) {
        Request memory request = requests[_reqId];
        require(now >= request.receivedTime.add(REQUEST_EXPIRE_TIME), "The request has not expired yet.");
        requests[_reqId].currentStage = Stage.Failed;
        request.dataConsumer.transfer(request.totalFee);
    }

    /**
     * @dev when there is something wrong accidentally when the calculation is running, 
     * @dev coordinator can cancel the MPC request and refund the data customer
     */
    function cancelMpcRequest(uint _reqId) external onlyCoordinator atStage(_reqId, Stage.Running) {
        Request memory request = requests[_reqId];
        requests[_reqId].currentStage = Stage.Failed;
        request.dataConsumer.transfer(request.totalFee);
    }
    
    function distributeRewards(uint _reqId) private {
        Request memory request = requests[_reqId];
        for (uint i = 0; i < request.dataProviderWorkers.length; i++) {
            payToVault(request.dataProviderWorkers[i], request.feeForDataProviderWorkers[i]);
        }
        for (uint i = 0; i < request.additionalWorkers.length; i++) {
            payToVault(request.additionalWorkers[i], request.feePerAdditionalWorker);
        }
        if (request.delegatedWorker != ADDRESS_NOT_ASSIGNED) {
            payToVault(request.delegatedWorker, request.feePerAdditionalWorker);
        }

    }
    
    function payToVault(address _payee, uint _amount) private {
        vault[_payee] = vault[_payee].add(_amount);
    }
    
    /**
     * @dev worker call this to withdraw all their rewards
     */
    function withdraw() external {
        uint amount = vault[msg.sender];
        msg.sender.transfer(amount);
        vault[msg.sender] = 0;
    }
    
    event WorkerListUpdated(address[] _workerList); // depends on the backend support the array, this may need to be changed
    event RequestReceived(
        uint _reqId,
        uint _numWorkers,
        string _programHash,
        address _dataConsumer,
        address[] _dataProviderWorkers,
        address _delegatedWorker,
        uint[] _feeForDataProviderWorkers,
        uint _feePerAdditionalWorker);
    event ComputationStarted(uint _reqId);
    event ResultPosted(uint _reqId, string _result);
}
