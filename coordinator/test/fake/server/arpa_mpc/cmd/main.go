package main

import (
	"context"
	"flag"
	"log"
	"math/rand"
	"net"
	"time"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	address    = flag.String("address", "127.0.0.1:10000", "The ip address and port number of the fake arpa_mpc server")
	blochchainAdapter = flag.String("blochchainAdapter", "127.0.0.1:40000", "The ip address and port number of the blochchainAdapter server")

	// TODO(John): enable configurable worker list
	workerList = []string{
		"0x7A6380C018E9DED3d41cc441A76fC58D803eB773",
		"0x605A9BeB2de5dC92Fe0Df269Ec43b810d8a5063E",
		"0x54C946765FE7553A26D91DdeBF3df90435700673",
		"0x709CEFa1EF25336a3e625E63F4aB886c267a184D",
		"0x15a306FAC0C6931aB93DD435d11B0d93B1326e0D",
		"0xf4742960e188763D1D5cF544d1f9b6dC8131D1cf",
		"0x8f1efA7939ad423416677eB3A4b176DB907e7cE8",
		"0xDa5cf2C3Fe7Bc1BF652eB842D2e6Aa18B13Fd493",
		"0xD165984bb3dB61C01274f80289BBa318d0aD4A7c",
		"0xEa5bb6B964DdE7442F78Ebe938766f361BA99188",
	}
)

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", *address)

	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	log.Printf("Server listening on : %v", *address)
	s := grpc.NewServer()

	arpaMPCServer := NewFakeArpaMPCServer()

	pb.RegisterArpaMpcServer(s, arpaMPCServer)

	log.Println("Starting fake ArpaMpc server...")
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}

type fakeArpaMPCServer struct{}

func NewFakeArpaMPCServer() pb.ArpaMpcServer {
	return &fakeArpaMPCServer{}
}

func (s *fakeArpaMPCServer) RunMpc(
	ctx context.Context,
	request *pb.RunMpcRequest,
) (*pb.RunMpcResponse, error) {
	mpcResult := &pb.MpcResult{
		Data: "mpc_result_from_fake_ArpaMpcServer",
	}
	mpcResults := make([]*pb.MpcResult, 0)
	mpcResults = append(mpcResults, mpcResult)
	response := &pb.RunMpcResponse{
		MpcFinalResult: mpcResults,
	}
	return response, nil
}

func (s *fakeArpaMPCServer) RunMpcForBlockchain(
	ctx context.Context,
	in *pb.RunMpcForBlockchainRequest,
) (*pb.RunMpcForBlockchainResponse, error) {

	const n = 10 // length of MpcId

	// generate a random MpcId
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	randId := make([]rune, n)
	for i := range randId {
		randId[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	mpcId := string(randId)

	var numAddWorker int
	if in.DelegatedWorkerBlockchainAddress == "" {
		numAddWorker = int(in.NumTotalWorkers) - len(in.DataProviderWorkerBlockchainAddresses)
	} else {
		numAddWorker = int(in.NumTotalWorkers) - len(in.DataProviderWorkerBlockchainAddresses) - 1
	}
	additionalWorkerList := workerList[:numAddWorker]

	res := &pb.RunMpcForBlockchainResponse{
		MpcId:                        mpcId,
		AdditionalWorkerBlockchainAddresses: additionalWorkerList,
	}

	// need to wait former tx to be mined, since currently we do not have failed and retry
	time.Sleep(time.Minute)

	go func() {
		// need to wait former tx to be mined, since currently we do not have failed and retry
		time.Sleep(time.Minute)
		conn, err := grpc.Dial(*blochchainAdapter, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("failed to start gRPC connection: %v", err)
		}
		log.Printf("created gRPC connection to server at %s", *blochchainAdapter)
		blochchainAdapterClient := pb.NewBlochchainAdapterClient(conn)
		defer conn.Close()

		mpcResult := &pb.MpcResult{
			Data: "mpc_result",
		}
		mpcResults := make([]*pb.MpcResult, 0)
		mpcResults = append(mpcResults, mpcResult)
		req := &pb.ReportMpcResultRequest{
			MpcId:  mpcId,
			Result: mpcResults,
		}
		res, err := blochchainAdapterClient.ReportMpcResult(context.Background(), req)
		if err != nil {
			log.Fatal(err)
		}
		if !res.Success {
			log.Fatalln("Report MPC result failed")
		}
		log.Println("Fake ArpaMpc server: MPC result reported")
	}()
	return res, nil
}
