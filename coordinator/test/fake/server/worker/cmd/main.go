package main

import (
	"context"
	"flag"
	"io/ioutil"
	"log"
	"net"
	"os"
	"time"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	workerAddress        = flag.String("waddr", "127.0.0.1:6000", "The IP address and Port of the worker server")
	coordinatorAddress   = flag.String("caddr", "127.0.0.1:10000", "The IP address and Port of the coordinator server")
	blockchainAddress    = flag.String("blockchain_addr", "0x0000000000000000000000000000000000000001", "The blockchain address of the worker server")
	csrPath              = flag.String("csrpath", "/src/gitlab.com/arpachain/mpc/worker/src/server/test_data/expected_csr.csr", "The path to fake csr.")
	simulatedMPCTime     = flag.Int("mpc_time", 3, "Simulated time for MPC in seconds.")
	testNetFakeResultMap = map[string]string{
		// testnet_advanced_arith
		"b5bf759c289b1405764303be2d182199664975ff5a9e1404e3978f44c33dad0f": "\nTesting secret values advanced arithmetic\na = 2.5 is a secret fixed point number.\nb = 3.4 is a secret fixed point number.\nc = 4.8 is a secret fixed point number.\nd = 1.2 is a secret fixed point number.\n\nTesting logarithm\nlog_2(a) = 1.32193.\n\nTesting exponentiation\nexp_2(b) = 10.5353.\n\nTesting power\n2^{3*log_2(c)} = 109.933.\n\nTesting general logarithm\nlog_6(d) = 0.101732.\n\nTesting ends\n",
		// testnet_basic_arith
		"4a9016baa5b0a199a328b6ca2df9d1587aa95e954a6ec52b1a36e42346bc644c": "\ntesting basic arithmetic operations\n\ntesting integer arithmetic\na = 5 is a secret integer.\nb = 7 is a secret integer.\na + b = 12.\na - b = -2.\na * b = 35.\nc = 11 is a clear integer\na * c = 55.\n\ntesting fixed point number arithmetic\nd = 90.415 is a secret fixed point number.\ne = 729.98 is a secret fixed point number.\nd + e = 820.395.\nd - e = -639.565.\nd * e = 66001.1.\nd / e = 0.123858.\nf = 17.93 is a clear fixed point number\nd * f = 1621.14.\ne / f = 40.7128.\n\ntesting mixed integer and fixed point number arithmetic\na * d = 452.075.\ne / b = 104.283.\n\ntesting floating point number arithmetic\ng = 123.45 is a secret floating point number.\nh = 67.89 is a secret floating point number.\ng + h = 191.34.\ng - h = 55.56.\ng * h = 8381.02.\ng / h = 1.81838.\n\ntesting mixed integer, fixed and floating point number arithmetic\nd * g = 11161.7.\ne / h = 10.7524.\n\ntesting end\n\n",
		// testnet_bit_op
		"33fd3ae6c1a83f382e223b1c78ae93d822e084ed6d3779806839b6a244ccdfdc": "\ntesting bit operations\na = 1 is a secret bit.\nb = 0 is a secret bit.\na or b is 1\na xor b is 1\n\nTesting bit reconstruction\nBits stream is 1110011111\nReconstructed bits is 927\n\nTesting binary summation\nBits stream a is 0110011111\nBits stream b is 1110011111\nBits stream summation is 10100111110\ntesting ends\n\n",
		// testnet_branching
		"449325631049754d8c905cc5d32cf664ba1d9021923f92295751d4c6963120a4": "\nTesting branching\nmem #0 stores 25\nmem #1 stores 35\n\nCondition = False\nFalse branch: mem #1 + 10\nmem result stores 45\n\nCondition = True\nTrue branch: mem #0 + 0\nmem result stores 25\n\\Testing ends\n",
		// testnet_lr_pred
		"646b6313fe4fedb8da2faeeb82d080033f4e1412825a6abea5da87ebc428a7e0": "\ntesting boston housing price prediction (linear regression)\n\nWeight array is as follows\n-0.153005, 0.0479393, -0.00860119, 2.57987, -14.6326, 3.96283, -0.00792313, -1.46078, 0.345431, -0.0124502, -0.919093, 0.0132208, -0.517256, \n\nBias is 36.4594\n\nOne sample in database is as follows\n0.11069, 0, 13.89, 1, 0.55, 5.951, 93.8, 2.8893, 5, 276, 16.4, 396.9, 17.92, \n\nTesting linear regression prediction\nPredicted price is 28.6698\nPredicted price is 27.7609\nPredicted price is 45.464\nPredicted price is 44.2534\nPredicted price is 33.4565\nPredicted price is 16.8708\nPredicted price is 20.3742\nPredicted price is 22.7652\nPredicted price is 25.5938\nPredicted price is 20.438\nPredicted price is 9.88026\nPredicted price is 28.348\nPredicted price is 34.7061\nPredicted price is 27.2489\nPredicted price is 23.3536\nPredicted price is 20.2121\nPredicted price is 25.5171\nPredicted price is 38.9846\nPredicted price is 30.2046\nPredicted price is 34.263\nPredicted price is 22.177\nPredicted price is 26.7052\nPredicted price is 33.4483\nPredicted price is 17.7084\nPredicted price is 39.0092\nPredicted price is 15.3821\nPredicted price is 17.9169\nPredicted price is 31.5606\nPredicted price is 35.9219\nPredicted price is 15.443\n\ntesting ends\n",
		// testnet_matrix_mult
		"2db8d92b02d56c80318b064b35784351de695b81f56eb2c72f937d544f1da130": "\n10 * 100 Matrix is as follow\n0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t\n0,\t1,\t2,\t3,\t4,\t5,\t6,\t7,\t8,\t9,\t10,\t11,\t12,\t13,\t14,\t15,\t16,\t17,\t18,\t19,\t20,\t21,\t22,\t23,\t24,\t25,\t26,\t27,\t28,\t29,\t30,\t31,\t32,\t33,\t34,\t35,\t36,\t37,\t38,\t39,\t40,\t41,\t42,\t43,\t44,\t45,\t46,\t47,\t48,\t49,\t50,\t51,\t52,\t53,\t54,\t55,\t56,\t57,\t58,\t59,\t60,\t61,\t62,\t63,\t64,\t65,\t66,\t67,\t68,\t69,\t70,\t71,\t72,\t73,\t74,\t75,\t76,\t77,\t78,\t79,\t80,\t81,\t82,\t83,\t84,\t85,\t86,\t87,\t88,\t89,\t90,\t91,\t92,\t93,\t94,\t95,\t96,\t97,\t98,\t99,\t\n0,\t2,\t4,\t6,\t8,\t10,\t12,\t14,\t16,\t18,\t20,\t22,\t24,\t26,\t28,\t30,\t32,\t34,\t36,\t38,\t40,\t42,\t44,\t46,\t48,\t50,\t52,\t54,\t56,\t58,\t60,\t62,\t64,\t66,\t68,\t70,\t72,\t74,\t76,\t78,\t80,\t82,\t84,\t86,\t88,\t90,\t92,\t94,\t96,\t98,\t100,\t102,\t104,\t106,\t108,\t110,\t112,\t114,\t116,\t118,\t120,\t122,\t124,\t126,\t128,\t130,\t132,\t134,\t136,\t138,\t140,\t142,\t144,\t146,\t148,\t150,\t152,\t154,\t156,\t158,\t160,\t162,\t164,\t166,\t168,\t170,\t172,\t174,\t176,\t178,\t180,\t182,\t184,\t186,\t188,\t190,\t192,\t194,\t196,\t198,\t\n0,\t3,\t6,\t9,\t12,\t15,\t18,\t21,\t24,\t27,\t30,\t33,\t36,\t39,\t42,\t45,\t48,\t51,\t54,\t57,\t60,\t63,\t66,\t69,\t72,\t75,\t78,\t81,\t84,\t87,\t90,\t93,\t96,\t99,\t102,\t105,\t108,\t111,\t114,\t117,\t120,\t123,\t126,\t129,\t132,\t135,\t138,\t141,\t144,\t147,\t150,\t153,\t156,\t159,\t162,\t165,\t168,\t171,\t174,\t177,\t180,\t183,\t186,\t189,\t192,\t195,\t198,\t201,\t204,\t207,\t210,\t213,\t216,\t219,\t222,\t225,\t228,\t231,\t234,\t237,\t240,\t243,\t246,\t249,\t252,\t255,\t258,\t261,\t264,\t267,\t270,\t273,\t276,\t279,\t282,\t285,\t288,\t291,\t294,\t297,\t\n0,\t4,\t8,\t12,\t16,\t20,\t24,\t28,\t32,\t36,\t40,\t44,\t48,\t52,\t56,\t60,\t64,\t68,\t72,\t76,\t80,\t84,\t88,\t92,\t96,\t100,\t104,\t108,\t112,\t116,\t120,\t124,\t128,\t132,\t136,\t140,\t144,\t148,\t152,\t156,\t160,\t164,\t168,\t172,\t176,\t180,\t184,\t188,\t192,\t196,\t200,\t204,\t208,\t212,\t216,\t220,\t224,\t228,\t232,\t236,\t240,\t244,\t248,\t252,\t256,\t260,\t264,\t268,\t272,\t276,\t280,\t284,\t288,\t292,\t296,\t300,\t304,\t308,\t312,\t316,\t320,\t324,\t328,\t332,\t336,\t340,\t344,\t348,\t352,\t356,\t360,\t364,\t368,\t372,\t376,\t380,\t384,\t388,\t392,\t396,\t\n0,\t5,\t10,\t15,\t20,\t25,\t30,\t35,\t40,\t45,\t50,\t55,\t60,\t65,\t70,\t75,\t80,\t85,\t90,\t95,\t100,\t105,\t110,\t115,\t120,\t125,\t130,\t135,\t140,\t145,\t150,\t155,\t160,\t165,\t170,\t175,\t180,\t185,\t190,\t195,\t200,\t205,\t210,\t215,\t220,\t225,\t230,\t235,\t240,\t245,\t250,\t255,\t260,\t265,\t270,\t275,\t280,\t285,\t290,\t295,\t300,\t305,\t310,\t315,\t320,\t325,\t330,\t335,\t340,\t345,\t350,\t355,\t360,\t365,\t370,\t375,\t380,\t385,\t390,\t395,\t400,\t405,\t410,\t415,\t420,\t425,\t430,\t435,\t440,\t445,\t450,\t455,\t460,\t465,\t470,\t475,\t480,\t485,\t490,\t495,\t\n0,\t6,\t12,\t18,\t24,\t30,\t36,\t42,\t48,\t54,\t60,\t66,\t72,\t78,\t84,\t90,\t96,\t102,\t108,\t114,\t120,\t126,\t132,\t138,\t144,\t150,\t156,\t162,\t168,\t174,\t180,\t186,\t192,\t198,\t204,\t210,\t216,\t222,\t228,\t234,\t240,\t246,\t252,\t258,\t264,\t270,\t276,\t282,\t288,\t294,\t300,\t306,\t312,\t318,\t324,\t330,\t336,\t342,\t348,\t354,\t360,\t366,\t372,\t378,\t384,\t390,\t396,\t402,\t408,\t414,\t420,\t426,\t432,\t438,\t444,\t450,\t456,\t462,\t468,\t474,\t480,\t486,\t492,\t498,\t504,\t510,\t516,\t522,\t528,\t534,\t540,\t546,\t552,\t558,\t564,\t570,\t576,\t582,\t588,\t594,\t\n0,\t7,\t14,\t21,\t28,\t35,\t42,\t49,\t56,\t63,\t70,\t77,\t84,\t91,\t98,\t105,\t112,\t119,\t126,\t133,\t140,\t147,\t154,\t161,\t168,\t175,\t182,\t189,\t196,\t203,\t210,\t217,\t224,\t231,\t238,\t245,\t252,\t259,\t266,\t273,\t280,\t287,\t294,\t301,\t308,\t315,\t322,\t329,\t336,\t343,\t350,\t357,\t364,\t371,\t378,\t385,\t392,\t399,\t406,\t413,\t420,\t427,\t434,\t441,\t448,\t455,\t462,\t469,\t476,\t483,\t490,\t497,\t504,\t511,\t518,\t525,\t532,\t539,\t546,\t553,\t560,\t567,\t574,\t581,\t588,\t595,\t602,\t609,\t616,\t623,\t630,\t637,\t644,\t651,\t658,\t665,\t672,\t679,\t686,\t693,\t\n0,\t8,\t16,\t24,\t32,\t40,\t48,\t56,\t64,\t72,\t80,\t88,\t96,\t104,\t112,\t120,\t128,\t136,\t144,\t152,\t160,\t168,\t176,\t184,\t192,\t200,\t208,\t216,\t224,\t232,\t240,\t248,\t256,\t264,\t272,\t280,\t288,\t296,\t304,\t312,\t320,\t328,\t336,\t344,\t352,\t360,\t368,\t376,\t384,\t392,\t400,\t408,\t416,\t424,\t432,\t440,\t448,\t456,\t464,\t472,\t480,\t488,\t496,\t504,\t512,\t520,\t528,\t536,\t544,\t552,\t560,\t568,\t576,\t584,\t592,\t600,\t608,\t616,\t624,\t632,\t640,\t648,\t656,\t664,\t672,\t680,\t688,\t696,\t704,\t712,\t720,\t728,\t736,\t744,\t752,\t760,\t768,\t776,\t784,\t792,\t\n0,\t9,\t18,\t27,\t36,\t45,\t54,\t63,\t72,\t81,\t90,\t99,\t108,\t117,\t126,\t135,\t144,\t153,\t162,\t171,\t180,\t189,\t198,\t207,\t216,\t225,\t234,\t243,\t252,\t261,\t270,\t279,\t288,\t297,\t306,\t315,\t324,\t333,\t342,\t351,\t360,\t369,\t378,\t387,\t396,\t405,\t414,\t423,\t432,\t441,\t450,\t459,\t468,\t477,\t486,\t495,\t504,\t513,\t522,\t531,\t540,\t549,\t558,\t567,\t576,\t585,\t594,\t603,\t612,\t621,\t630,\t639,\t648,\t657,\t666,\t675,\t684,\t693,\t702,\t711,\t720,\t729,\t738,\t747,\t756,\t765,\t774,\t783,\t792,\t801,\t810,\t819,\t828,\t837,\t846,\t855,\t864,\t873,\t882,\t891,\t\n\n100 * 10 Matrix is as follow\n50,\t51,\t52,\t53,\t54,\t55,\t56,\t57,\t58,\t59,\t\n49,\t50,\t51,\t52,\t53,\t54,\t55,\t56,\t57,\t58,\t\n48,\t49,\t50,\t51,\t52,\t53,\t54,\t55,\t56,\t57,\t\n47,\t48,\t49,\t50,\t51,\t52,\t53,\t54,\t55,\t56,\t\n46,\t47,\t48,\t49,\t50,\t51,\t52,\t53,\t54,\t55,\t\n45,\t46,\t47,\t48,\t49,\t50,\t51,\t52,\t53,\t54,\t\n44,\t45,\t46,\t47,\t48,\t49,\t50,\t51,\t52,\t53,\t\n43,\t44,\t45,\t46,\t47,\t48,\t49,\t50,\t51,\t52,\t\n42,\t43,\t44,\t45,\t46,\t47,\t48,\t49,\t50,\t51,\t\n41,\t42,\t43,\t44,\t45,\t46,\t47,\t48,\t49,\t50,\t\n40,\t41,\t42,\t43,\t44,\t45,\t46,\t47,\t48,\t49,\t\n39,\t40,\t41,\t42,\t43,\t44,\t45,\t46,\t47,\t48,\t\n38,\t39,\t40,\t41,\t42,\t43,\t44,\t45,\t46,\t47,\t\n37,\t38,\t39,\t40,\t41,\t42,\t43,\t44,\t45,\t46,\t\n36,\t37,\t38,\t39,\t40,\t41,\t42,\t43,\t44,\t45,\t\n35,\t36,\t37,\t38,\t39,\t40,\t41,\t42,\t43,\t44,\t\n34,\t35,\t36,\t37,\t38,\t39,\t40,\t41,\t42,\t43,\t\n33,\t34,\t35,\t36,\t37,\t38,\t39,\t40,\t41,\t42,\t\n32,\t33,\t34,\t35,\t36,\t37,\t38,\t39,\t40,\t41,\t\n31,\t32,\t33,\t34,\t35,\t36,\t37,\t38,\t39,\t40,\t\n30,\t31,\t32,\t33,\t34,\t35,\t36,\t37,\t38,\t39,\t\n29,\t30,\t31,\t32,\t33,\t34,\t35,\t36,\t37,\t38,\t\n28,\t29,\t30,\t31,\t32,\t33,\t34,\t35,\t36,\t37,\t\n27,\t28,\t29,\t30,\t31,\t32,\t33,\t34,\t35,\t36,\t\n26,\t27,\t28,\t29,\t30,\t31,\t32,\t33,\t34,\t35,\t\n25,\t26,\t27,\t28,\t29,\t30,\t31,\t32,\t33,\t34,\t\n24,\t25,\t26,\t27,\t28,\t29,\t30,\t31,\t32,\t33,\t\n23,\t24,\t25,\t26,\t27,\t28,\t29,\t30,\t31,\t32,\t\n22,\t23,\t24,\t25,\t26,\t27,\t28,\t29,\t30,\t31,\t\n21,\t22,\t23,\t24,\t25,\t26,\t27,\t28,\t29,\t30,\t\n20,\t21,\t22,\t23,\t24,\t25,\t26,\t27,\t28,\t29,\t\n19,\t20,\t21,\t22,\t23,\t24,\t25,\t26,\t27,\t28,\t\n18,\t19,\t20,\t21,\t22,\t23,\t24,\t25,\t26,\t27,\t\n17,\t18,\t19,\t20,\t21,\t22,\t23,\t24,\t25,\t26,\t\n16,\t17,\t18,\t19,\t20,\t21,\t22,\t23,\t24,\t25,\t\n15,\t16,\t17,\t18,\t19,\t20,\t21,\t22,\t23,\t24,\t\n14,\t15,\t16,\t17,\t18,\t19,\t20,\t21,\t22,\t23,\t\n13,\t14,\t15,\t16,\t17,\t18,\t19,\t20,\t21,\t22,\t\n12,\t13,\t14,\t15,\t16,\t17,\t18,\t19,\t20,\t21,\t\n11,\t12,\t13,\t14,\t15,\t16,\t17,\t18,\t19,\t20,\t\n10,\t11,\t12,\t13,\t14,\t15,\t16,\t17,\t18,\t19,\t\n9,\t10,\t11,\t12,\t13,\t14,\t15,\t16,\t17,\t18,\t\n8,\t9,\t10,\t11,\t12,\t13,\t14,\t15,\t16,\t17,\t\n7,\t8,\t9,\t10,\t11,\t12,\t13,\t14,\t15,\t16,\t\n6,\t7,\t8,\t9,\t10,\t11,\t12,\t13,\t14,\t15,\t\n5,\t6,\t7,\t8,\t9,\t10,\t11,\t12,\t13,\t14,\t\n4,\t5,\t6,\t7,\t8,\t9,\t10,\t11,\t12,\t13,\t\n3,\t4,\t5,\t6,\t7,\t8,\t9,\t10,\t11,\t12,\t\n2,\t3,\t4,\t5,\t6,\t7,\t8,\t9,\t10,\t11,\t\n1,\t2,\t3,\t4,\t5,\t6,\t7,\t8,\t9,\t10,\t\n0,\t1,\t2,\t3,\t4,\t5,\t6,\t7,\t8,\t9,\t\n-1,\t0,\t1,\t2,\t3,\t4,\t5,\t6,\t7,\t8,\t\n-2,\t-1,\t0,\t1,\t2,\t3,\t4,\t5,\t6,\t7,\t\n-3,\t-2,\t-1,\t0,\t1,\t2,\t3,\t4,\t5,\t6,\t\n-4,\t-3,\t-2,\t-1,\t0,\t1,\t2,\t3,\t4,\t5,\t\n-5,\t-4,\t-3,\t-2,\t-1,\t0,\t1,\t2,\t3,\t4,\t\n-6,\t-5,\t-4,\t-3,\t-2,\t-1,\t0,\t1,\t2,\t3,\t\n-7,\t-6,\t-5,\t-4,\t-3,\t-2,\t-1,\t0,\t1,\t2,\t\n-8,\t-7,\t-6,\t-5,\t-4,\t-3,\t-2,\t-1,\t0,\t1,\t\n-9,\t-8,\t-7,\t-6,\t-5,\t-4,\t-3,\t-2,\t-1,\t0,\t\n-10,\t-9,\t-8,\t-7,\t-6,\t-5,\t-4,\t-3,\t-2,\t-1,\t\n-11,\t-10,\t-9,\t-8,\t-7,\t-6,\t-5,\t-4,\t-3,\t-2,\t\n-12,\t-11,\t-10,\t-9,\t-8,\t-7,\t-6,\t-5,\t-4,\t-3,\t\n-13,\t-12,\t-11,\t-10,\t-9,\t-8,\t-7,\t-6,\t-5,\t-4,\t\n-14,\t-13,\t-12,\t-11,\t-10,\t-9,\t-8,\t-7,\t-6,\t-5,\t\n-15,\t-14,\t-13,\t-12,\t-11,\t-10,\t-9,\t-8,\t-7,\t-6,\t\n-16,\t-15,\t-14,\t-13,\t-12,\t-11,\t-10,\t-9,\t-8,\t-7,\t\n-17,\t-16,\t-15,\t-14,\t-13,\t-12,\t-11,\t-10,\t-9,\t-8,\t\n-18,\t-17,\t-16,\t-15,\t-14,\t-13,\t-12,\t-11,\t-10,\t-9,\t\n-19,\t-18,\t-17,\t-16,\t-15,\t-14,\t-13,\t-12,\t-11,\t-10,\t\n-20,\t-19,\t-18,\t-17,\t-16,\t-15,\t-14,\t-13,\t-12,\t-11,\t\n-21,\t-20,\t-19,\t-18,\t-17,\t-16,\t-15,\t-14,\t-13,\t-12,\t\n-22,\t-21,\t-20,\t-19,\t-18,\t-17,\t-16,\t-15,\t-14,\t-13,\t\n-23,\t-22,\t-21,\t-20,\t-19,\t-18,\t-17,\t-16,\t-15,\t-14,\t\n-24,\t-23,\t-22,\t-21,\t-20,\t-19,\t-18,\t-17,\t-16,\t-15,\t\n-25,\t-24,\t-23,\t-22,\t-21,\t-20,\t-19,\t-18,\t-17,\t-16,\t\n-26,\t-25,\t-24,\t-23,\t-22,\t-21,\t-20,\t-19,\t-18,\t-17,\t\n-27,\t-26,\t-25,\t-24,\t-23,\t-22,\t-21,\t-20,\t-19,\t-18,\t\n-28,\t-27,\t-26,\t-25,\t-24,\t-23,\t-22,\t-21,\t-20,\t-19,\t\n-29,\t-28,\t-27,\t-26,\t-25,\t-24,\t-23,\t-22,\t-21,\t-20,\t\n-30,\t-29,\t-28,\t-27,\t-26,\t-25,\t-24,\t-23,\t-22,\t-21,\t\n-31,\t-30,\t-29,\t-28,\t-27,\t-26,\t-25,\t-24,\t-23,\t-22,\t\n-32,\t-31,\t-30,\t-29,\t-28,\t-27,\t-26,\t-25,\t-24,\t-23,\t\n-33,\t-32,\t-31,\t-30,\t-29,\t-28,\t-27,\t-26,\t-25,\t-24,\t\n-34,\t-33,\t-32,\t-31,\t-30,\t-29,\t-28,\t-27,\t-26,\t-25,\t\n-35,\t-34,\t-33,\t-32,\t-31,\t-30,\t-29,\t-28,\t-27,\t-26,\t\n-36,\t-35,\t-34,\t-33,\t-32,\t-31,\t-30,\t-29,\t-28,\t-27,\t\n-37,\t-36,\t-35,\t-34,\t-33,\t-32,\t-31,\t-30,\t-29,\t-28,\t\n-38,\t-37,\t-36,\t-35,\t-34,\t-33,\t-32,\t-31,\t-30,\t-29,\t\n-39,\t-38,\t-37,\t-36,\t-35,\t-34,\t-33,\t-32,\t-31,\t-30,\t\n-40,\t-39,\t-38,\t-37,\t-36,\t-35,\t-34,\t-33,\t-32,\t-31,\t\n-41,\t-40,\t-39,\t-38,\t-37,\t-36,\t-35,\t-34,\t-33,\t-32,\t\n-42,\t-41,\t-40,\t-39,\t-38,\t-37,\t-36,\t-35,\t-34,\t-33,\t\n-43,\t-42,\t-41,\t-40,\t-39,\t-38,\t-37,\t-36,\t-35,\t-34,\t\n-44,\t-43,\t-42,\t-41,\t-40,\t-39,\t-38,\t-37,\t-36,\t-35,\t\n-45,\t-44,\t-43,\t-42,\t-41,\t-40,\t-39,\t-38,\t-37,\t-36,\t\n-46,\t-45,\t-44,\t-43,\t-42,\t-41,\t-40,\t-39,\t-38,\t-37,\t\n-47,\t-46,\t-45,\t-44,\t-43,\t-42,\t-41,\t-40,\t-39,\t-38,\t\n-48,\t-47,\t-46,\t-45,\t-44,\t-43,\t-42,\t-41,\t-40,\t-39,\t\n-49,\t-48,\t-47,\t-46,\t-45,\t-44,\t-43,\t-42,\t-41,\t-40,\t\n\nMatrix multiplication result is\n0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t0,\t\n-80850,\t-75900,\t-70950,\t-66000,\t-61050,\t-56100,\t-51150,\t-46200,\t-41250,\t-36300,\t\n-161700,\t-151800,\t-141900,\t-132000,\t-122100,\t-112200,\t-102300,\t-92400,\t-82500,\t-72600,\t\n-242550,\t-227700,\t-212850,\t-198000,\t-183150,\t-168300,\t-153450,\t-138600,\t-123750,\t-108900,\t\n-323400,\t-303600,\t-283800,\t-264000,\t-244200,\t-224400,\t-204600,\t-184800,\t-165000,\t-145200,\t\n-404250,\t-379500,\t-354750,\t-330000,\t-305250,\t-280500,\t-255750,\t-231000,\t-206250,\t-181500,\t\n-485100,\t-455400,\t-425700,\t-396000,\t-366300,\t-336600,\t-306900,\t-277200,\t-247500,\t-217800,\t\n-565950,\t-531300,\t-496650,\t-462000,\t-427350,\t-392700,\t-358050,\t-323400,\t-288750,\t-254100,\t\n-646800,\t-607200,\t-567600,\t-528000,\t-488400,\t-448800,\t-409200,\t-369600,\t-330000,\t-290400,\t\n-727650,\t-683100,\t-638550,\t-594000,\t-549450,\t-504900,\t-460350,\t-415800,\t-371250,\t-326700,\t\n",
		// testnet_sorting
		"a298fd6f8c61f2e60384cb84437d0406e5575b7c95f45ef8f36f74e0bbb217a3": "\nTesting sorting\n\nThe queue before sorting is\n68, 51, -15, -48, 2, -19, 56, -39, -4, 16, 81, 0, -43, 51, 23, -49, \n\nAfter sorting\n-49, -48, -43, -39, -19, -15, -4, 0, 2, 16, 23, 51, 51, 56, 68, 81, \n",
		// testnet_stat
		"9758dbe256264f143700574a389a8eb66c0bb35106d29adc82c6b0d7653db8f9": "\nTesting statistics\n\nSamples of dataset\nCRIM,\tZN,\tINDUS,\tCHAS,\tNOX,\tRM,\tAGE,\tDIS,\tRAD,\tTAX,\tPTRATIO,\tB,\tLSTAT,\t\n0.11069,\t0,\t13.89,\t1,\t0.55,\t5.951,\t93.8,\t2.8893,\t5,\t276,\t16.4,\t396.9,\t17.92,\t\n0.15098,\t0,\t10.01,\t0,\t0.547,\t6.021,\t82.6,\t2.7474,\t6,\t432,\t17.8,\t394.51,\t10.3,\t\n0.0138102,\t80,\t0.46,\t0,\t0.422,\t7.875,\t32,\t5.6484,\t4,\t255,\t14.4,\t394.23,\t2.97,\t\n1.83377,\t0,\t19.58,\t1,\t0.605,\t7.802,\t98.2,\t2.0407,\t5,\t403,\t14.7,\t389.61,\t1.92,\t\n0.11425,\t0,\t13.89,\t1,\t0.55,\t6.373,\t92.4,\t3.3633,\t5,\t276,\t16.4,\t393.74,\t10.5,\t\nCRIM\nAverage is 3.92722.\nNOX\nAverage is 0.568035.\nStandard deviation is 0.61762.\n",
		// testnet_trigonometry
		"3b8e83ca8dc9fa3875232857cda89dbfe0a3538d3bd42bb8db8c7fcb9d5ef89f": "\nTesting trigonometry on secret values\na = -7.5 is a secret fixed point number\nb = -4.5 is a secret fixed point number\nc = 3 is a secret fixed point number\nd = 6 is a secret fixed point number\n\ntesting sin computation\nsin of a is -0.938001\n\ntesting cos computation\ncos of b is -0.210804\n\ntesting tan computation\ntan of c is -0.142547\n\ntesting atan computation\natan of d is 1.40565\n\ntesting asin computation\ne = 0.1 is a secret fixed point number\nf = 0.5 is a secret fixed point number\ng = -0.3 is a secret fixed point number\nh = -0.9 is a secret fixed point number\n\nasin of e is 0.100168\n\ntesting acos computation\nacos of f is 1.0472\n\nTesting ends\n",
	}
)

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", *workerAddress)

	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	log.Printf("Server listening on : %v", *workerAddress)
	s := grpc.NewServer()

	conn, err := grpc.Dial(*coordinatorAddress, grpc.WithInsecure())
	if err != nil {
		log.Printf("did not connect to coordinator[%v]: %v", *coordinatorAddress, err)
		return
	}
	defer conn.Close()
	client := pb.NewCoordinatorClient(conn)

	simulatedTime := time.Duration(*simulatedMPCTime) * time.Second
	mpcWorkerServer := NewFakeMpcWorkerServer(simulatedTime, *workerAddress, client, testNetFakeResultMap)
	mpcWorkerServer.Register(*blockchainAddress)

	pb.RegisterMpcWorkerServer(s, mpcWorkerServer)

	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}

type FakeMpcWorkerServer struct {
	simulatedMPCTime time.Duration
	nodeID           string
	nodeName         string
	address          string
	cert             []byte
	client           pb.CoordinatorClient
	request          *pb.StartMpcRequest
	fakeResultMap    map[string]string
}

func NewFakeMpcWorkerServer(
	simulatedMPCTime time.Duration,
	address string,
	client pb.CoordinatorClient,
	fakeResultMap map[string]string,
) *FakeMpcWorkerServer {
	server := &FakeMpcWorkerServer{
		simulatedMPCTime: simulatedMPCTime,
		address:          address,
		client:           client,
		fakeResultMap:    fakeResultMap,
	}
	return server
}

func (s *FakeMpcWorkerServer) StartMpc(ctx context.Context, req *pb.StartMpcRequest) (*pb.StartMpcResponse, error) {
	log.Printf("calling fake StartMpc(...)")
	go s.finishMPC()
	s.request = req
	return &pb.StartMpcResponse{
		Success: true,
	}, nil
}

func (s *FakeMpcWorkerServer) Heartbeat(ctx context.Context, req *pb.HeartbeatRequest) (*pb.HeartbeatResponse, error) {
	log.Printf("calling Heartbeat(...) on fake node: %v", s.nodeID)
	return &pb.HeartbeatResponse{
		Healthy: true,
	}, nil
}

func (s *FakeMpcWorkerServer) finishMPC() {
	// sleep some time to mimic the computation time-lapse
	time.Sleep(s.simulatedMPCTime)
	fakeMPCResultData, exists := s.fakeResultMap[s.request.Program.ProgramHash]
	if !exists {
		log.Printf("error: didn't found fake result for program hash %v", s.request.Program.ProgramHash)
		// Didn't find a proper fake mpc result. Return empty result.
		fakeMPCResultData = ""
	}
	fakeMPCResult := &pb.MpcResult{
		Data: fakeMPCResultData,
	}
	req := &pb.FinishMpcRequest{
		MpcId:  s.request.MpcId,
		NodeId: s.nodeID,
		Result: fakeMPCResult,
	}
	log.Printf("fake mpc finished, calling FinishMpc(...)")
	res, err := s.client.FinishMpc(context.Background(), req)
	if err != nil {
		log.Printf("calling FinishMpc(...) failed: %v", err)
		return
	}
	log.Printf("calling FinishMpc(...) succeeded with status: %v", res.Success)
}

func (s *FakeMpcWorkerServer) Register(blockchainAddress string) error {
	csrFullPath := os.Getenv("GOPATH") + *csrPath
	csr, err := ioutil.ReadFile(csrFullPath)
	if err != nil {
		return err
	}
	req := &pb.RegisterMpcNodeRequest{
		Csr:               csr,
		NodeName:          "fake_node",
		Address:           s.address,
		BlockchainAddress: blockchainAddress,
	}
	res, err := s.client.RegisterMpcNode(context.Background(), req)
	if err != nil {
		return err
	}
	s.nodeID = res.GetNodeId()
	s.cert = res.GetCert()
	log.Printf("Node registered with ID: %v", s.nodeID)
	return nil
}
