package server

import (
	"context"
	"log"
	"time"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	grpcpool "github.com/processout/grpc-go-pool"
	"google.golang.org/grpc"
)

// BlochchainAdapterClientFactory uses factory pattern to materialize (re-use) BlochchainAdapterClient and gRPC connetion
type BlochchainAdapterClientFactory interface {
	GetBlochchainAdapterClient(ctx context.Context) (pb.BlochchainAdapterClient, *grpcpool.ClientConn, error)
}

// Reference implementation:

type blochchainAdapterClientFactory struct {
	blochchainAdapterServerAddress string
	clientPool              *grpcpool.Pool
	initialNumClients       int
	poolCapacity            int
	idleTimeout             time.Duration
}

// NewBlochchainAdapterFactory returns an instance of server.BlochchainAdapterClientFactory
func NewBlochchainAdapterFactory(blochchainAdapterServerAddress string) BlochchainAdapterClientFactory {
	return &blochchainAdapterClientFactory{
		blochchainAdapterServerAddress: blochchainAdapterServerAddress,
		initialNumClients:       5,
		poolCapacity:            10,
		idleTimeout:             time.Minute,
	}
}

func (f *blochchainAdapterClientFactory) GetBlochchainAdapterClient(ctx context.Context) (pb.BlochchainAdapterClient, *grpcpool.ClientConn, error) {
	var err error
	if f.clientPool == nil {
		f.clientPool, err = grpcpool.New(f.getConnection(f.blochchainAdapterServerAddress), f.initialNumClients, f.poolCapacity, f.idleTimeout)
		if err != nil {
			return nil, nil, err
		}
	}
	connection, err := f.clientPool.Get(ctx)
	if err != nil {
		return nil, nil, err
	}
	return pb.NewBlochchainAdapterClient(connection.ClientConn), connection, nil
}

// this is a helper function for returning a func that the pool.New(...) uses
// when instantiating a pool
func (f *blochchainAdapterClientFactory) getConnection(serverAddress string) func() (*grpc.ClientConn, error) {
	return func() (*grpc.ClientConn, error) {
		conn, err := grpc.Dial(serverAddress, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("failed to start gRPC connection: %v", err)
		}
		log.Printf("created gRPC connection to server at %s", serverAddress)
		return conn, err
	}
}
