package server

import (
	"context"
	"fmt"
	"log"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/mpc"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
)

// coordinatorServer is used to implement Coordinator
type coordinatorServer struct {
	nodeManager         node.Manager
	mpcManager          mpc.Manager
	mpcResultChannelMap *map[string]chan MPCResult // mpcID -> result chan
}

// TODO(John): this is not true anymore for other blochchain, need to add flags
const (
	validBlockchainAddressStrLen = 42
)

// NewCoordinatorServer returns an instance of pb.CoordinatorServer
func NewCoordinatorServer(
	nodeManager node.Manager,
	mpcManager mpc.Manager,
	mpcResultChannelMap *map[string]chan MPCResult,
) pb.CoordinatorServer {
	return &coordinatorServer{
		nodeManager:         nodeManager,
		mpcManager:          mpcManager,
		mpcResultChannelMap: mpcResultChannelMap,
	}
}

// RegisterMpcNode implements Coordinator.RegisterMpcNode
func (s *coordinatorServer) RegisterMpcNode(
	ctx context.Context,
	in *pb.RegisterMpcNodeRequest,
) (*pb.RegisterMpcNodeResponse, error) {
	if len(in.GetBlockchainAddress()) != 0 && len(in.GetBlockchainAddress()) != validBlockchainAddressStrLen {
		return &pb.RegisterMpcNodeResponse{}, fmt.Errorf("ERROR: Invalid Eth Address: %v", in.GetBlockchainAddress())
	}
	nodeID, certificate, err := s.nodeManager.Register(
		in.GetAddress(), in.GetNodeName(), in.GetCsr(), in.GetBlockchainAddress())

	if err != nil {
		log.Printf("Node[Name:%v|ID:%v] registration failed for reason: %v", in.NodeName, nodeID, err)
		return &pb.RegisterMpcNodeResponse{}, err
	}
	log.Printf("Node[Name:%v|ID:%v] is registered, with address: [%v]", in.NodeName, nodeID, in.GetAddress())
	return &pb.RegisterMpcNodeResponse{
		NodeId:  nodeID,
		Cert:    certificate,
		Success: true,
	}, nil
}

// FinishMpc implements Coordinator.FinishMpc
func (s *coordinatorServer) FinishMpc(
	ctx context.Context,
	in *pb.FinishMpcRequest,
) (*pb.FinishMpcResponse, error) {
	mpcResult := &pb.MpcResult{
		Data: in.GetResult().GetData(),
	}
	err := s.mpcManager.Finish(in.GetMpcId(), in.GetNodeId(), mpcResult)

	if resultChannel, ok := (*s.mpcResultChannelMap)[in.GetMpcId()]; ok {
		log.Printf("Received MPC result for MPC ID: %v with result: %v", in.GetMpcId(), in.GetResult().GetData())
		resultChannel <- MPCResult{err, in.GetMpcId(), in.Result}
	} else {
		log.Printf("Received MPC result that no one waits for. MPC ID: %v", in.GetMpcId())
	}

	return &pb.FinishMpcResponse{
		Success: err == nil,
	}, err
}
