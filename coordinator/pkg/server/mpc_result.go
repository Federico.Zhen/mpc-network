package server

import (
	pb "gitlab.com/arpachain/mpc/coordinator/api"
)

type MPCResult struct {
	Err   error
	MpcID string
	// nodeIndex int : What's the index of the node in an instance of MPC, will be needed soon
	MpcResult *pb.MpcResult
}
