package blockchain

import (
	"fmt"
	"sync"
)

// Mapper for BlockchainAddress => NodeID
type Mapper interface {
	Add(blockchainAddress, nodeID string) error
	LookUp(blockchainAddress string) (string /*NodeID*/, error)
	Remove(blockchainAddress string)
}

// Reference implementation:

type mapper struct {
	mu       *sync.Mutex
	mappings map[string]string /*key: BlockchainAddress value: NodeID*/
}

func NewMapper() Mapper {
	return &mapper{
		mu:       &sync.Mutex{},
		mappings: make(map[string]string),
	}
}

func (m *mapper) LookUp(blockchainAddress string) (string /*NodeID*/, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	if nodeID, exists := m.mappings[blockchainAddress]; exists {
		return nodeID, nil
	}
	return "", fmt.Errorf("node for address: %v does not exist", blockchainAddress)
}

func (m *mapper) Remove(blockchainAddress string) {
	m.mu.Lock()
	defer m.mu.Unlock()
	delete(m.mappings, blockchainAddress)
}

func (m *mapper) Add(blockchainAddress, nodeID string) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	if _, exists := m.mappings[blockchainAddress]; exists {
		return fmt.Errorf("mapping with same address already exists: %v", blockchainAddress)
	}
	m.mappings[blockchainAddress] = nodeID
	return nil
}
