package worker_updater_test

import (
	"context"
	"testing"
	"time"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	mock_pb "gitlab.com/arpachain/mpc/coordinator/mock/api"
	"gitlab.com/arpachain/mpc/coordinator/mock/node"
	"gitlab.com/arpachain/mpc/coordinator/mock/server"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"gitlab.com/arpachain/mpc/coordinator/pkg/worker_updater"
	"gitlab.com/arpachain/mpc/coordinator/test"
	"github.com/golang/mock/gomock"
	"github.com/processout/grpc-go-pool"
)

var (
	numNodes           = 5
	expectedNumUpdates = 3

	offset   = 30 * time.Millisecond
	interval = 100 * time.Millisecond
	waitTime = (interval + offset) * time.Duration(expectedNumUpdates)
)

func TestStart(t *testing.T) {
	controller := gomock.NewController(t)

	allNodes := make([]node.Node, 0)
	for i := 0; i < numNodes; i++ {
		allNodes = append(allNodes, *test_util.GenerateTestNode())
	}
	repository := mock_node.NewMockRepository(controller)
	client := mock_pb.NewMockBlochchainAdapterClient(controller)
	factory := mock_server.NewMockBlochchainAdapterClientFactory(controller)
	setMockNodeRepositoryBehavior_Valid(repository, allNodes)
	setMockBlochchainAdapterClientFactoryBehavior_Valid(factory, client)

	client.
		EXPECT().
		UpdateWorkerList(gomock.Any(), gomock.Any()).
		Return(&pb.UpdateWorkerListResponse{Success: true}, nil).
		Times(expectedNumUpdates)
	updater := worker_updater.NewUpdater(repository, factory)
	// Start the updater so it sends update on a certain interval.
	updater.Start(context.Background(), interval)
	// Wait enough time to make sure client.UpdateWorkerList(...)
	// gets called for expected number of times (expectedNumUpdates).
	time.Sleep(waitTime)
}

func setMockBlochchainAdapterClientFactoryBehavior_Valid(f *mock_server.MockBlochchainAdapterClientFactory, c *mock_pb.MockBlochchainAdapterClient) {
	f.
		EXPECT().
		GetBlochchainAdapterClient(gomock.Any()).
		Return(c, &grpcpool.ClientConn{}, nil).
		AnyTimes()
}

func setMockNodeRepositoryBehavior_Valid(r *mock_node.MockRepository, nodes []node.Node) {
	r.
		EXPECT().
		GetAll().
		Return(nodes, nil).
		AnyTimes()
}
