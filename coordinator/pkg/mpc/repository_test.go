package mpc_test

import (
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/mpc"
	tu "gitlab.com/arpachain/mpc/coordinator/test"
	"github.com/stretchr/testify/assert"
)

const (
	numNodes = 5
	numMPC   = 5
)

func TestCreateAndGetByID(t *testing.T) {
	assert := assert.New(t)
	repository := mpc.NewRepository()

	t.Log("case 1: testing Create...")
	fakeNodeIDs := tu.GenerateNodeIDs(numNodes)
	id, err := repository.Create(tu.GenerateMPC(fakeNodeIDs))
	assert.NoError(err)
	assert.NotEmpty(id)

	t.Log("case 2: testing GetByID...")
	createdMPC, err := repository.GetByID(id)
	assert.NoError(err)
	assert.NotEmpty(createdMPC)
	assert.Equal(createdMPC.ID, id)
	assert.Equal(len(createdMPC.NodeIDs), numNodes, "ensure the mpc has all the nodes")
	assert.Empty(createdMPC.ResultMap, "newly created mpc should not have results")
	for _, flag := range createdMPC.FinishFlagMap {
		assert.False(flag, "all finish flags should be false for newly created mpc")
	}
	assert.False(createdMPC.IsFinished(), "newly created mpc should not be finished")

	t.Log("case 3: testing GetByID with an invalid id...")
	createdMPC, err = repository.GetByID("invalid_id")
	assert.Error(err, "should fail for invalid id")
	assert.Empty(createdMPC, "should return an empty node")
}

func TestFinishMPC(t *testing.T) {
	assert := assert.New(t)
	repository := mpc.NewRepository()

	t.Log("creating a testing mpc... assuming creation and retrieval always work")
	// arbitrarily choosing n number of node ids in this mpc
	fakeNodeIDs := tu.GenerateNodeIDs(numNodes)
	id, _ := repository.Create(tu.GenerateMPC(fakeNodeIDs))
	mpc, _ := repository.GetByID(id)
	assert.False(mpc.IsFinished(), "mpc should be finished initially")

	t.Log("case 1: testing FinishMPC...")
	// arbitrarily picking the first node id
	nodeIDToFinish := fakeNodeIDs[0]
	fakeMPCResult := &arpa_mpc.MpcResult{
		Data: "fake_data",
	}
	err := repository.FinishMPC(id, nodeIDToFinish, fakeMPCResult)
	assert.NoError(err, "there should be no error when finishing an mpc")
	mpc, _ = repository.GetByID(id)
	assert.True(mpc.FinishFlagMap[nodeIDToFinish], "the finish flag for the specified node should be true")
	assert.False(mpc.IsFinished(), "mpc should not be finished yet")

	t.Log("case 2: testing FinishMPC on a already finished mpc...")
	err = repository.FinishMPC(id, nodeIDToFinish, fakeMPCResult)
	assert.Error(err, "should fail since you cannot finish an already finished mpc")

	t.Log("case 3: testing FinishMPC with an invalid id...")
	err = repository.FinishMPC("invalid_id", nodeIDToFinish, fakeMPCResult)
	assert.Error(err, "should fail for invalid id")

	t.Log("case 4: testing FinishMPC for all nodes...")
	// starting from the second node id in fakeNodeIDs generated above
	// since we have already called 'FinishMPC' on that one
	for i := 1; i < numNodes; i++ {
		nodeID := fakeNodeIDs[i]
		err := repository.FinishMPC(id, nodeID, fakeMPCResult)
		assert.NoError(err, "should be no errors")
	}
	mpc, _ = repository.GetByID(id)
	assert.True(mpc.IsFinished(), "entire mpc should be finished")
}

func TestGetAll(t *testing.T) {
	assert := assert.New(t)
	repository := mpc.NewRepository()

	t.Log("case 1: testing GetAll when there is no mpc...")
	mpcList, err := repository.GetAll()
	assert.Empty(mpcList, "should return empty MPC")

	t.Log("case 2: testing GetAll when there are n mpcList...")
	t.Log("creating n MPC to the empty repository...")
	tu.CreateMPC(numMPC, repository)
	mpcList, err = repository.GetAll()
	assert.NoError(err, "retrieval should be successful")
	assert.Equal(len(mpcList), numMPC, "there should be same number of mpc retrieved")
}

func TestRemove(t *testing.T) {
	assert := assert.New(t)
	repository := mpc.NewRepository()
	fakeNodeIDs := tu.GenerateNodeIDs(numNodes)
	id, _ := repository.Create(tu.GenerateMPC(fakeNodeIDs))

	t.Log("case 1: testing Remove...")
	mpc, _ := repository.GetByID(id)
	assert.NotEmpty(mpc, "making sure the mpc is there")
	repository.Remove(id)
	mpc, _ = repository.GetByID(id)
	assert.Empty(mpc, "the node should be removed thus returned empty mpc object")
}
