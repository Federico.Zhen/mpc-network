package mpc

import (
	"gitlab.com/arpachain/mpc/coordinator/api"
)

// Manager for mpc provides necessary functions for mpc server to interact with each multi-party computation
type Manager interface {
	GetByIDs(ids []string) ([]*MPC, error)
	Create(nodeIDs []string) (string, error)
	Finish(mpcID string, nodeID string, mpcResult *arpa_mpc.MpcResult) error
}

// Reference implementation:

type manager struct {
	repository Repository
}

// NewManager returns an instance of mpc.Manager
func NewManager(repository Repository) Manager {
	return &manager{
		repository: repository,
	}
}

func (m *manager) GetByIDs(ids []string) ([]*MPC, error) {
	mpcToReturn := make([]*MPC, len(ids))
	for i, mpcID := range ids {
		mpc, err := m.repository.GetByID(mpcID)
		if err != nil {
			return nil, err
		}
		mpcToReturn[i] = &mpc
	}
	return mpcToReturn, nil
}

func (m *manager) Create(nodeIDs []string) (string, error) {
	resultMap := make(map[string]arpa_mpc.MpcResult)
	finishFlagMap := make(map[string]bool)
	for _, nodeID := range nodeIDs {
		finishFlagMap[nodeID] = false
	}
	mpc := MPC{
		NodeIDs:       nodeIDs,
		FinishFlagMap: finishFlagMap,
		ResultMap:     resultMap,
	}
	return m.repository.Create(&mpc)
}

func (m *manager) Finish(
	mpcID string,
	nodeID string,
	mpcResult *arpa_mpc.MpcResult,
) error {
	return m.repository.FinishMPC(mpcID, nodeID, mpcResult)
}
