package mpc

import (
	"errors"
	"fmt"
	"sync"

	"gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/util"
)

// Repository for mpc provides CRUD operations to persist mpc data
type Repository interface {
	GetAll() ([]MPC, error)
	GetByID(id string) (MPC, error)
	FinishMPC(mpcID, nodeID string, mpcResult *arpa_mpc.MpcResult) error
	Create(mpc *MPC) (string, error)
	Remove(id string)
}

// Reference implementation:

type repository struct {
	mu     *sync.Mutex
	mpcMap map[string]MPC
}

// NewRepository returns an instance of mpc.Repository
func NewRepository() Repository {
	return &repository{
		mu:     &sync.Mutex{},
		mpcMap: map[string]MPC{},
	}
}

func (r *repository) GetAll() ([]MPC, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	mpcList := make([]MPC, 0)
	for _, mpc := range r.mpcMap {
		mpcList = append(mpcList, mpc)
	}
	return mpcList, nil
}

func (r *repository) GetByID(id string) (MPC, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	if mpc, exists := r.mpcMap[id]; exists {
		return mpc, nil
	}
	return MPC{}, fmt.Errorf("mpc does not exist: %v", id)
}

func (r *repository) FinishMPC(mpcID, nodeID string, mpcResult *arpa_mpc.MpcResult) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	if mpc, exists := r.mpcMap[mpcID]; exists {
		if mpc.FinishFlagMap[nodeID] {
			return fmt.Errorf("error: node %v has already finished its part of mpc", nodeID)
		}
		mpc.FinishFlagMap[nodeID] = true
		mpc.ResultMap[nodeID] = *mpcResult
		r.mpcMap[mpcID] = mpc
		return nil
	}
	return fmt.Errorf("finish MPC failed, mpc does not exist: %v", mpcID)
}

func (r *repository) Create(mpc *MPC) (string, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	id, err := util.GenerateID()
	if err != nil {
		return "", err
	}
	if _, exists := r.mpcMap[id]; exists {
		return "", errors.New("mpc with same id already exists")
	}
	r.mpcMap[id] = MPC{
		ID:            id,
		NodeIDs:       mpc.NodeIDs,
		FinishFlagMap: mpc.FinishFlagMap,
		ResultMap:     mpc.ResultMap,
	}
	return id, nil
}

func (r *repository) Remove(id string) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.mpcMap, id)
}
