package program

type Program struct {
	ID         string /* the program hash */
	ProgramMap map[string][]byte
	Schedule   string
}
