package group

import (
	"errors"
	"fmt"

	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
)

type sequentialManager struct {
	nodeRepository  node.Repository
	groupRepository Repository
}

// NewSequentialManager returns an instance of group.Manager
func NewSequentialManager(
	nodeRepository node.Repository,
	groupRepository Repository,
) Manager {
	return &sequentialManager{
		nodeRepository:  nodeRepository,
		groupRepository: groupRepository,
	}
}

func (m *sequentialManager) CreateGroup(size int) (string /*GroupID*/, error) {
	allNodes, err := m.nodeRepository.GetAll()
	if err != nil {
		return "", err
	}
	if len(allNodes) < size {
		return "", errors.New("not enough nodes")
	}

	availableNodes := make([]node.Node, 0)
	for _, node := range allNodes {
		if !node.IsGrouped {
			availableNodes = append(availableNodes, node)
		}
	}
	if len(availableNodes) < size {
		return "", errors.New("not enough available nodes")
	}

	// pick n (n = size) nodes in sequence:
	pickedNodeIDs := make([]string, 0)
	for _, pickedNode := range availableNodes[:size] {
		m.nodeRepository.GroupNode(pickedNode.ID)
		pickedNodeIDs = append(pickedNodeIDs, pickedNode.ID)
	}
	newGroup := &Group{
		NodeIDs: pickedNodeIDs,
	}
	id, err := m.groupRepository.Create(newGroup)
	if err != nil {
		return "", err
	}
	return id, nil
}

func (m *sequentialManager) Ungroup(groupIDs []string) error {
	if len(groupIDs) < 1 {
		return errors.New("no group id")
	}
	for _, groupID := range groupIDs {
		group, err := m.groupRepository.GetByID(groupID)
		if err != nil {
			return err
		}
		nodeIDs := group.NodeIDs
		for _, nodeID := range nodeIDs {
			node, err := m.nodeRepository.GetByID(nodeID)
			if err != nil {
				return err
			}
			m.nodeRepository.UngroupNode(node.ID)
		}
		m.groupRepository.Remove(groupID)
	}
	return nil
}

func (m *sequentialManager) AddToGroup(groupID string, nodeIDs []string) error {
	group, err := m.groupRepository.GetByID(groupID)
	if err != nil {
		return err
	}
	if group.IsBusy {
		return errors.New("cannot add nodes to a busy group")
	}
	for _, nodeID := range nodeIDs {
		node, err := m.nodeRepository.GetByID(nodeID)
		if err != nil {
			return err
		}
		if node.IsGrouped {
			return fmt.Errorf("cannot add already grouped node: %v", node.ID)
		}
		err = m.nodeRepository.GroupNode(nodeID)
		if err != nil {
			return err
		}
	}
	oldNodeIDs := group.NodeIDs
	newNodeIDs := append(oldNodeIDs, nodeIDs...)
	return m.groupRepository.UpdateNodeIDs(group.ID, newNodeIDs)
}

func (m *sequentialManager) GetNodesFromGroup(groupID string) ([]*node.Node, error) {
	group, err := m.groupRepository.GetByID(groupID)
	if err != nil {
		return nil, err
	}
	nodeIDs := group.NodeIDs
	nodes := make([]*node.Node, len(nodeIDs))
	for i, nodeID := range nodeIDs {
		node, err := m.nodeRepository.GetByID(nodeID)
		if err != nil {
			return nil, err
		}
		nodes[i] = &node
	}
	return nodes, err
}

// TODO(bomo): free groups should be released
func (m *sequentialManager) GetAvailableGroup(size int) (*Group, error) {
	group, err := m.groupRepository.GetAvailableGroup(size)
	if err != nil {
		newGroupID, err := m.CreateGroup(size)
		if err != nil {
			return nil, err
		}
		group, err = m.groupRepository.GetByID(newGroupID)
	}
	err = m.groupRepository.UpdateIsBusy(group.ID, true /*mark the group as busy*/)
	group.IsBusy = true
	if err != nil {
		return nil, err
	}
	return &group, nil
}

func (m *sequentialManager) GetAvailableGroupForBlockchain(
	dataNodeIDs []string,
	delegatedNodeID string,
	numAdditionalNodes int,
) (*Group, []string /*Additional NodeIDs*/, error) {
	requestedNodeIDs := make([]string, 0)
	// The below guard clauses guarantees that
	// delegated node and data nodes exist and are not grouped
	if len(delegatedNodeID) > 0 {
		delegatedNode, err := m.nodeRepository.GetByID(delegatedNodeID)
		if err != nil {
			return nil, nil, err
		}
		if delegatedNode.IsGrouped {
			return nil, nil, fmt.Errorf("cannot get available group, delegated node: %v is in another group", delegatedNodeID)
		}
		requestedNodeIDs = append(requestedNodeIDs, delegatedNodeID)
	}
	for _, dataNodeID := range dataNodeIDs {
		dataNode, err := m.nodeRepository.GetByID(dataNodeID)
		if err != nil {
			return nil, nil, err
		}
		if dataNode.IsGrouped {
			return nil, nil, fmt.Errorf("cannot get available group, data node: %v is in another group", dataNodeID)
		}
		requestedNodeIDs = append(requestedNodeIDs, dataNodeID)
	}

	// creates a group with requested delegated node and data nodes first
	id, err := m.groupRepository.Create(
		&Group{
			NodeIDs: requestedNodeIDs,
		})
	if err != nil {
		return nil, nil, err
	}
	for _, nodeID := range requestedNodeIDs {
		err := m.nodeRepository.GroupNode(nodeID)
		if err != nil {
			return nil, nil, err
		}
	}

	// add n additional nodes to the group
	allNodes, err := m.nodeRepository.GetAll()
	if err != nil {
		return nil, nil, err
	}
	if len(allNodes) < numAdditionalNodes {
		return nil, nil, errors.New("not enough nodes")
	}
	availableNodes := make([]node.Node, 0)
	for _, node := range allNodes {
		if !node.IsGrouped {
			availableNodes = append(availableNodes, node)
		}
	}
	if len(availableNodes) < numAdditionalNodes {
		return nil, nil, errors.New("not enough available nodes")
	}

	// pick n (n = size) nodes in sequence:
	pickedNodeIDs := make([]string, 0)
	for _, pickedNode := range availableNodes[:numAdditionalNodes] {
		m.nodeRepository.GroupNode(pickedNode.ID)
		pickedNodeIDs = append(pickedNodeIDs, pickedNode.ID)
	}

	updatedNodeIDs := append(requestedNodeIDs, pickedNodeIDs...)
	err = m.groupRepository.UpdateNodeIDs(id, updatedNodeIDs)
	if err != nil {
		return nil, nil, err
	}
	group, err := m.groupRepository.GetByID(id)
	return &group, pickedNodeIDs, err
}
