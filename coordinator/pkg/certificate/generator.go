package certificate

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"io/ioutil"
	"math/big"
	"os"
	"time"
)

var (
	goPath                  = os.Getenv("GOPATH")
	defaultCAPublicKeyPath  = goPath + "/src/gitlab.com/arpachain/mpc/coordinator/pkg/certificate/test_certificates/ca.crt"
	defaultCAPrivateKeyPath = goPath + "/src/gitlab.com/arpachain/mpc/coordinator/pkg/certificate/test_certificates/ca.key"
)

// Generator is used to generate a client certificate from a CSR
type Generator interface {
	GenerateCertificate(
		clientCSR []byte,
		pathToRootCertificate,
		pathToCAPrivateKey string,
	) ([]byte, error)
	GetCACertificate(path string) ([]byte, error)
}

// Reference Implementation:

type generator struct{}

// NewGenerator returns an instance of certificate.Generator
func NewGenerator() Generator {
	return &generator{}
}

func (g *generator) GenerateCertificate(
	clientCSR []byte,
	pathToRootCertificate,
	pathToCAPrivateKey string,
) ([]byte, error) {
	caCertificate, err := loadCACertificate(pathToRootCertificate)
	if err != nil {
		return nil, err
	}
	caPrivateKey, err := loadCAPrivateKey(pathToCAPrivateKey)
	if err != nil {
		return nil, err
	}
	clientCertificate, err := generateCertificate(clientCSR, caCertificate, caPrivateKey)
	if err != nil {
		return nil, err
	}
	return clientCertificate, nil
}

func (g *generator) GetCACertificate(path string) ([]byte, error) {
	var caPublicKey []byte
	var err error
	if path == "" {
		path = defaultCAPublicKeyPath
	}
	if caPublicKey, err = ioutil.ReadFile(path); err != nil {
		return nil, err
	}
	pemBlock, _ := pem.Decode(caPublicKey)
	if pemBlock == nil {
		return nil, errors.New("decoding ca root certificate failed")
	}
	certificate := pem.EncodeToMemory(pemBlock)
	return certificate, nil
}

func loadCACertificate(pathToRootCertificate string) (*x509.Certificate, error) {
	var caPublicKey []byte
	var err error
	if pathToRootCertificate == "" {
		pathToRootCertificate = defaultCAPublicKeyPath
	}
	if caPublicKey, err = ioutil.ReadFile(pathToRootCertificate); err != nil {
		return nil, err

	}
	pemBlock, _ := pem.Decode(caPublicKey)
	if pemBlock == nil {
		return nil, errors.New("decoding ca root certificate failed")
	}
	caRootCertificate, err := x509.ParseCertificate(pemBlock.Bytes)
	if err != nil {
		return nil, err
	}
	return caRootCertificate, nil
}

func loadCAPrivateKey(pathToCAPrivateKey string) (*rsa.PrivateKey, error) {
	var caPrivateKey []byte
	var err error
	if pathToCAPrivateKey == "" {
		pathToCAPrivateKey = defaultCAPrivateKeyPath
	}
	if caPrivateKey, err = ioutil.ReadFile(pathToCAPrivateKey); err != nil {
		return nil, err
	}
	pemblock, _ := pem.Decode(caPrivateKey)
	if pemblock == nil {
		return nil, errors.New("decoding ca private key failed")
	}
	parsedKey, err := x509.ParsePKCS1PrivateKey(pemblock.Bytes)
	if err != nil {
		return nil, err
	}
	return parsedKey, nil
}

func generateCertificate(
	clientCSR []byte,
	caRootCertificate *x509.Certificate,
	caPrivateKey *rsa.PrivateKey,
) ([]byte, error) {
	pemBlock, _ := pem.Decode(clientCSR)
	if pemBlock == nil {
		return nil, errors.New("decoding client csr failed")
	}
	request, err := x509.ParseCertificateRequest(pemBlock.Bytes)
	if err != nil {
		return nil, err
	}
	if err = request.CheckSignature(); err != nil {
		return nil, err
	}
	clientCRTTemplate := x509.Certificate{
		Signature:          request.Signature,
		SignatureAlgorithm: request.SignatureAlgorithm,

		PublicKeyAlgorithm: request.PublicKeyAlgorithm,
		PublicKey:          request.PublicKey,

		SerialNumber: big.NewInt(2),
		Issuer:       caRootCertificate.Subject,
		Subject:      request.Subject,
		NotBefore:    time.Now(),
		NotAfter:     time.Now().Add(30 * 24 * time.Hour), // expired action should be a todo
		KeyUsage:     x509.KeyUsageDigitalSignature,
		ExtKeyUsage:  []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth},
	}
	// create client certificate from template and CA public key
	clientCertificateRaw, err := x509.CreateCertificate(
		rand.Reader,
		&clientCRTTemplate,
		caRootCertificate,
		request.PublicKey,
		caPrivateKey,
	)
	if err != nil {
		return nil, err
	}
	clientCertificate := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: clientCertificateRaw})
	return clientCertificate, nil
}
