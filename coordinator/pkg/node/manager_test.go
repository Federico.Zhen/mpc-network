package node_test

import (
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/mock/blockchain"
	"gitlab.com/arpachain/mpc/coordinator/mock/certificate"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	tu "gitlab.com/arpachain/mpc/coordinator/test"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestRegister(t *testing.T) {
	assert := assert.New(t)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	generator := mock_certificate.NewMockGenerator(ctrl)
	tu.SetupMockCertificateGenerator(generator)

	mapper := mock_blockchain.NewMockMapper(ctrl)
	mapper.
		EXPECT().
		Add(gomock.Any(), gomock.Any()).
		Return(nil).
		AnyTimes()

	repository := node.NewRepository()
	manager := node.NewManager(repository, mapper, generator)

	address := "127.0.0.1:6001"
	name := "registered_node"
	csr := []byte("fake_csr")
	crt := []byte("fake_crt")

	t.Log("case 1: testing Register...")
	id, crt, err := manager.Register(address, name, csr, "0xC2D7CF95645D33006175B78989035C7c9061d3F9")
	assert.NoError(err, "register node should be successful")
	assert.NotEmpty(id, "id should be returned")
	assert.NotEmpty(crt, "certificate should be returned")
	node, err := repository.GetByID(id)
	assert.NoError(err, "registered node should be retrievable")
	assert.NotEmpty(node, "registered node should be successfully retrieved")
	assert.Equal(address, node.Address, "ip should be the same")
	assert.Equal(name, node.Name, "name should be the same")
	assert.Equal(false, node.IsGrouped, "newly registered node should not be grouped")
	assert.Equal(crt, node.Certificate, "certificate should be the same")
}

func TestGetByIDs(t *testing.T) {
	assert := assert.New(t)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	generator := mock_certificate.NewMockGenerator(ctrl)
	tu.SetupMockCertificateGenerator(generator)
	mapper := mock_blockchain.NewMockMapper(ctrl)

	repository := node.NewRepository()
	manager := node.NewManager(repository, mapper, generator)

	t.Log("create n nodes")
	ids := tu.CreateNodes(numNodes, repository)

	t.Log("case 1: testing GetByIDs...")
	nodes, err := manager.GetByIDs(ids)
	assert.NoError(err)
	assert.NotEmpty(nodes)
	assert.Equal(len(nodes), numNodes)
}
