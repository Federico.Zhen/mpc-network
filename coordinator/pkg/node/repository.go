package node

import (
	"errors"
	"sync"

	"gitlab.com/arpachain/mpc/coordinator/pkg/util"
)

// Repository for node provides CRUD operations to persist node data
type Repository interface {
	GetAll() ([]Node, error)
	GetByID(id string) (Node, error)
	Create(node *Node) (string, error)
	GroupNode(id string) error
	UngroupNode(id string) error
	Remove(id string)
}

// Reference implementation:

type repository struct {
	mu      *sync.Mutex
	nodeMap map[string]Node /*key: NodeID*/
}

// NewRepository returns an instance of node.Repository
func NewRepository() Repository {
	return &repository{
		mu:      &sync.Mutex{},
		nodeMap: map[string]Node{},
	}
}

func (r *repository) GetAll() ([]Node, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	nodes := make([]Node, 0)
	for _, node := range r.nodeMap {
		nodes = append(nodes, node)
	}
	return nodes, nil
}

func (r *repository) GetByID(id string) (Node, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	if node, exists := r.nodeMap[id]; exists {
		return node, nil
	}
	return Node{}, errors.New("node does not exist")
}

func (r *repository) Create(node *Node) (string, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	id, err := util.GenerateID()
	if err != nil {
		return "", err
	}
	if _, exists := r.nodeMap[id]; exists {
		return "", errors.New("node with same id already exists")
	}
	r.nodeMap[id] = Node{
		ID:          id,
		Name:        node.Name,
		Address:     node.Address,
		IsGrouped:   node.IsGrouped,
		Certificate: node.Certificate,
		BlockchainAddress:  node.BlockchainAddress,
	}
	return id, nil
}

func (r *repository) UngroupNode(id string) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	if node, exists := r.nodeMap[id]; exists {
		if node.IsGrouped {
			node.IsGrouped = false
			r.nodeMap[id] = node
			return nil
		}
		return errors.New("this node is not grouped")
	}
	return errors.New("node does not exist")
}

func (r *repository) GroupNode(id string) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	if node, exists := r.nodeMap[id]; exists {
		if node.IsGrouped {
			return errors.New("this node is already grouped")
		}
		node.IsGrouped = true
		r.nodeMap[id] = node
		return nil
	}
	return errors.New("node does not exist")
}

func (r *repository) Remove(id string) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.nodeMap, id)
}
