package node

import (
	"errors"
	"sync"

	"gitlab.com/arpachain/mpc/coordinator/pkg/util"
)

type sequentialRepository struct {
	mu                *sync.Mutex
	nodeMap           map[string]Node /*key: NodeID*/
	sequentialNodeIDs []string        /*to keep track of the sequence of node registration*/
}

// NewSequentialRepository returns an instance of node.Repository
func NewSequentialRepository() Repository {
	return &sequentialRepository{
		mu:                &sync.Mutex{},
		nodeMap:           map[string]Node{},
		sequentialNodeIDs: make([]string, 0),
	}
}

func (r *sequentialRepository) GetAll() ([]Node, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	nodes := make([]Node, 0)
	for _, nodeID := range r.sequentialNodeIDs {
		node := r.nodeMap[nodeID]
		nodes = append(nodes, node)
	}
	return nodes, nil
}

func (r *sequentialRepository) GetByID(id string) (Node, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	if node, exists := r.nodeMap[id]; exists {
		return node, nil
	}
	return Node{}, errors.New("node does not exist")
}

func (r *sequentialRepository) Create(node *Node) (string, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	id, err := util.GenerateID()
	if err != nil {
		return "", err
	}
	if _, exists := r.nodeMap[id]; exists {
		return "", errors.New("node with same id already exists")
	}
	r.nodeMap[id] = Node{
		ID:          id,
		Name:        node.Name,
		Address:     node.Address,
		IsGrouped:   node.IsGrouped,
		Certificate: node.Certificate,
	}
	r.sequentialNodeIDs = append(r.sequentialNodeIDs, id)
	return id, nil
}

func (r *sequentialRepository) UngroupNode(id string) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	if node, exists := r.nodeMap[id]; exists {
		if node.IsGrouped {
			node.IsGrouped = false
			r.nodeMap[id] = node
			return nil
		}
		return errors.New("this node is not grouped")
	}
	return errors.New("node does not exist")
}

func (r *sequentialRepository) GroupNode(id string) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	if node, exists := r.nodeMap[id]; exists {
		if node.IsGrouped {
			return errors.New("this node is already grouped")
		}
		node.IsGrouped = true
		r.nodeMap[id] = node
		return nil
	}
	return errors.New("node does not exist")
}

func (r *sequentialRepository) Remove(id string) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.nodeMap, id)
}
