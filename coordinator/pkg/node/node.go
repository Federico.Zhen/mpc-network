package node

// Node as a single MPC node
type Node struct {
	ID          string
	Name        string
	Address     string
	IsGrouped   bool
	Certificate []byte
	BlockchainAddress  string
}
