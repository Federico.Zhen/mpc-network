#!/bin/bash

set -ex
cd $GOPATH/src/gitlab.com/arpachain/mpc/coordinator/cmd/coordinator/
go run main.go
