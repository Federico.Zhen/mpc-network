package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc"
)

var (
	goPath         = os.Getenv("GOPATH")
	defaultPath    = goPath + "/src/gitlab.com/arpachain/mpc/coordinator/caller/test_data"
	address        = flag.String("address", "127.0.0.1:10000", "The ip address and port number of the arpa_mpc server")
	path           = flag.String("path", defaultPath, "The path to the directory that contains the test data files.")
	numNodes       = flag.Int("numNodes", 3, "The number of nodes.")
	includeMPCData = flag.Bool("includeMpc", false, "If true, the coordinator server will distribute mpc data, this is only for testing")
	programHash    = flag.String("program_hash", "4a9016baa5b0a199a328b6ca2df9d1587aa95e954a6ec52b1a36e42346bc644c", "Program hash computed from MPC program. Default: testnet_basic_arith")
)

func main() {
	flag.Parse()
	conn, err := grpc.Dial(*address, grpc.WithInsecure())
	if err != nil {
		fmt.Printf("could not connect to arpa mpc server [%v]: %v\n", *address, err)
		return
	}
	defer conn.Close()
	c := pb.NewArpaMpcClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Hour)
	defer cancel()

	reader := Reader{
		Path: *path,
	}

	request := &pb.RunMpcRequest{}
	if *includeMPCData {
		request, err = reader.ReadMPCDataFromProtoTxt()
		if err != nil {
			fmt.Printf("could not read mpc data file: %v\n", err)
			return
		}
	}
	request.NumNodes = uint32(*numNodes)
	request.ProgramHash = *programHash

	fmt.Printf("calling RunMPC(...) on server [%v]\n", *address)
	fmt.Println("[BEGIN] RunMpcRequest [BEGIN]")
	fmt.Println(proto.MarshalTextString(request))
	fmt.Println("[ END ] RunMpcRequest [ END ]")

	response, err := c.RunMpc(ctx, request)
	if err != nil {
		fmt.Printf("could not start mpc: %v\n", err)
		return
	}
	fmt.Printf("mpc finished: %v\n", response.MpcFinalResult)
}

type Reader struct {
	Path string
}

func (r *Reader) ReadProgBin() (map[string][]byte, error) {
	resultMap := make(map[string][]byte)
	dir := r.Path + string(filepath.Separator)
	d, err := os.Open(dir)
	defer d.Close()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	files, err := d.Readdir(-1)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	fmt.Println("Reading " + dir)
	for _, file := range files {
		fn := file.Name()
		if file.Mode().IsRegular() {
			if filepath.Ext(fn) == ".bc" {
				fmt.Println(fn)
				content, err := ioutil.ReadFile(r.Path + "/" + fn)
				if err != nil {
					fmt.Println(err)
					return nil, err
				}
				resultMap[fn[:len(fn)-3]] = content
			}
		}
	}
	if len(resultMap) < 1 {
		return nil, errors.New("mpc program file not found")
	}
	return resultMap, nil
}

func (r *Reader) ReadSch() (string, error) {
	dir := r.Path + string(filepath.Separator)
	d, err := os.Open(dir)
	defer d.Close()
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	files, err := d.Readdir(-1)
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	fmt.Println("Reading " + dir)
	for _, file := range files {
		fn := file.Name()
		if file.Mode().IsRegular() {
			if filepath.Ext(fn) == ".sch" {
				fmt.Println(fn)
				content, err := ioutil.ReadFile(r.Path + "/" + fn)
				if err != nil {
					fmt.Println(err)
					return "", err
				}
				return string(content), nil
			}
		}
	}
	return "", errors.New("mpc schedule file not found")
}

func (r *Reader) ReadMPCDataFromProtoTxt() (*pb.RunMpcRequest, error) {
	pbMsg := &pb.RunMpcRequest{}
	dir := r.Path + string(filepath.Separator)
	d, err := os.Open(dir)
	defer d.Close()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	files, err := d.Readdir(-1)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	fmt.Println("Reading " + dir)
	for _, file := range files {
		fn := file.Name()

		if file.Mode().IsRegular() {
			if filepath.Ext(fn) == ".prototxt" {
				fmt.Println(fn)
				protoBytes, err := ioutil.ReadFile(r.Path + "/" + fn)
				err = proto.UnmarshalText(string(protoBytes), pbMsg)
				if err != nil {
					fmt.Println(err)
					return nil, err
				}
				return pbMsg, nil
			}
		}
	}
	return nil, fmt.Errorf("mpc data file not found at: %v", r.Path)
}
