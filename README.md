# ARPA MPC

[![CircleCI](https://circleci.com/gh/arpachain/mpc/tree/master.svg?style=shield&circle-token=e04f28789a71c24ba30eaa4dd253eb8b009b36de)](https://circleci.com/gh/arpachain/mpc/tree/master)

### Setup

After cloning this repository, first init submodules by:

```
  git submodule init
  git submodule update
```

To test everything, run

```
  make check
```

To build everything, run

```
  make
```

To clean up all files generated by build, run

```
  make clean
```

### Instructions for Docker

##### Official Docker Image:

https://hub.docker.com/r/arpachainio/mpc/

(Note: you might need `sudo` for the following commands)

To pull our docker image:

```bash
docker pull arpachainio/mpc
```

To list all of the docker images (for identifying image name or id):

```bash
docker images
```

To run the image with an interactive `bash` shell:

`-i` means interactive mode

`-t` allocates a pseudo TTY

```bash
docker run -it <image_name_or_id> /bin/bash
```

To check container status:

```bash
docker container ls -a
```

To stop a running container:

```bash
docker stop <container_name_or_id>
```

To start a stopped container:

```bash
docker start <container_name_or_id>
```

To connect to a running container with an interactive `bash` shell:

```bash
docker exec -it <container_name_or_id> /bin/bash
```
