package mpc

import (
	"encoding/json"
	"fmt"
	"log"

	"gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/mpc"
	"gitlab.com/arpachain/mpc/persistent/pkg/db"
	"github.com/dgraph-io/badger"
)

const (
	keyPrefix = "mpc_"
)

// BadgerRepository implements interface:
// gitlab.com/arpachain/mpc/coordinator/pkg/mpc/Repository
type badgerRepository struct {
	db *badger.DB
}

// NewBadgerRepository returns a BadgerDB-based implementation of interface:
// gitlab.com/arpachain/mpc/coordinator/pkg/mpc/Repository
func NewBadgerRepository(db *badger.DB) mpc.Repository {
	return &badgerRepository{
		db: db,
	}
}

func (r *badgerRepository) GetAll() ([]mpc.MPC, error) {
	mpcList := make([]mpc.MPC, 0)
	objects, err := db_utils.GetAllObjects(r.db, keyPrefix, &mpc.MPC{})
	if err != nil {
		return nil, err
	}
	for _, obj := range objects {
		m := obj.(*mpc.MPC)
		mpcList = append(mpcList, *m)
	}
	return mpcList, nil
}

// GetByID returns empty mpc and throws error when id does not exist
func (r *badgerRepository) GetByID(id string) (mpc.MPC, error) {
	var m mpc.MPC
	exists, err := db_utils.GetObject(r.db, keyPrefix, id, &m)
	if err != nil {
		return mpc.MPC{}, err
	}
	if !exists {
		return mpc.MPC{}, fmt.Errorf("the mpc with id: %v does not exist", id)
	}
	return m, nil
}

func (r *badgerRepository) FinishMPC(mpcID, nodeID string, mpcResult *arpa_mpc.MpcResult) error {
	return r.readAndUpdate(mpcID,
		func(oldMPC *mpc.MPC) (*mpc.MPC, error) {
			if oldMPC.FinishFlagMap[nodeID] {
				return nil, fmt.Errorf("error: node %v has already finished its part of mpc %v", nodeID, mpcID)
			}
			oldMPC.FinishFlagMap[nodeID] = true
			oldMPC.ResultMap[nodeID] = *mpcResult
			return oldMPC, nil
		})
}

func (r *badgerRepository) Create(mpc *mpc.MPC) (string, error) {
	mpcBytes, err := json.Marshal(mpc)
	if err != nil {
		return "", err
	}
	err = db_utils.CreateObject(r.db, keyPrefix, mpc.ID, mpcBytes)
	if err != nil {
		return "", err
	}
	return mpc.ID, nil
}

func (r *badgerRepository) Remove(id string) {
	err := db_utils.DeleteObject(r.db, keyPrefix, id)
	// TODO(bomo) need to change the interface behavior for error handling
	if err != nil {
		log.Printf("deleting mpc : %v failed for reason: %v", id, err)
	}
}

func (r *badgerRepository) readAndUpdate(
	id string,
	updateFn func(*mpc.MPC) (*mpc.MPC, error),
) error {
	err := r.db.Update(func(txn *badger.Txn) error {
		var oldMPC mpc.MPC
		item, err := txn.Get([]byte(keyPrefix + id))
		if err != nil {
			return err
		}
		err = item.Value(func(v []byte) error {
			err := json.Unmarshal(v, &oldMPC)
			return err
		})
		if err != nil {
			return err
		}
		newMPC, err := updateFn(&oldMPC)
		if err != nil {
			return err
		}
		mpcBytes, err := json.Marshal(newMPC)
		if err != nil {
			return err
		}
		return txn.Set([]byte(keyPrefix+newMPC.ID), mpcBytes)
	})
	return err
}
