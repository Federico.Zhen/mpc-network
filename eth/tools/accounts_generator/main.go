// Generate a batch of accounts (private key and its corresponding address)
// that is compatible to ethereum network
package main

import (
	"flag"
	"fmt"
	"log"

	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/pkg/util"
	"github.com/ethereum/go-ethereum/crypto"
)

var numAccount = flag.Int("n", 5, "Number of eth accounts to generate")

func printAccount(a model.Account) {
	fmt.Printf("Private key: %x\n", a.PrivateKey.D.Bytes())
	fmt.Printf("Address: %v\n", a.Address.Hex())
	fmt.Println()
}

func generateRandomAccount() (model.Account, error) {
	sk, err := crypto.GenerateKey()
	if err != nil {
		return model.Account{}, err
	}
	addr, err := util.GetAddrFromSk(sk)
	if err != nil {
		return model.Account{}, err
	}
	return model.Account{sk, addr}, nil
}

func main() {
	flag.Parse()
	for i := 0; i < *numAccount; i++ {
		fmt.Printf("The %dth account:\n", i+1)
		account, err := generateRandomAccount()
		if err != nil {
			log.Panic(err)
		}
		printAccount(account)
	}
}
