#!/bin/bash

set -ex

PACKAGE_PATH=gitlab.com/arpachain/mpc/eth
ETH_PATH=$GOPATH/src/$PACKAGE_PATH

mockgen \
  -source=$ETH_PATH/pkg/request/repository.go \
  -destination=$ETH_PATH/mock/request/mock_repository.go

mockgen \
  -source=$ETH_PATH/pkg/worker_list/repository.go \
  -destination=$ETH_PATH/mock/worker_list/mock_repository.go

mockgen \
  -source=$ETH_PATH/pkg/transaction/tx_opts.go \
  -destination=$ETH_PATH/mock/transaction/mock_tx_opts.go

mockgen \
  -source=$ETH_PATH/pkg/parser/parser.go \
  -destination=$ETH_PATH/mock/parser/mock_parser.go

mockgen \
  -source=$ETH_PATH/pkg/abstract/eth_client.go \
  -destination=$ETH_PATH/mock/abstract/mock_eth_client.go

mockgen \
  -destination=$ETH_PATH/mock/api/mock_arpa_mpc_client.go \
  -package=mock_arpa_mpc \
  $PACKAGE_PATH/api ArpaMpcClient
