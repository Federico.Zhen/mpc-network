#!/bin/bash

set -ex

SOL_SRC_DIR=$GOPATH/src/gitlab.com/arpachain/mpc/smart_contract/contracts
SOL_LIB_DIR=$GOPATH/src/gitlab.com/arpachain/mpc/smart_contract/
SOL_DST_DIR=$GOPATH/src/gitlab.com/arpachain/mpc/eth/sol

mkdir -p $SOL_DST_DIR
solc \
  --allow-paths $SOL_LIB_DIR \
  --abi \
  --bin \
  $SOL_SRC_DIR/ArpaMpc.sol \
  -o $SOL_DST_DIR \
  --overwrite
abigen \
  --abi=$SOL_DST_DIR/ArpaMpc.abi \
  --bin=$SOL_DST_DIR/ArpaMpc.bin \
  --pkg=contract \
  --out=$SOL_DST_DIR/ArpaMpc.sol.go
