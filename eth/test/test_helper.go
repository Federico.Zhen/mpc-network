package test

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	"os"

	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/pkg/transaction"
	"gitlab.com/arpachain/mpc/eth/pkg/util"
	contract "gitlab.com/arpachain/mpc/eth/sol"
	"gitlab.com/arpachain/mpc/eth/test/data"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
)

const (
	Coordinator int = iota
	DataProviderWorker
	User
	Worker1
	Worker2
	DelegatedWorker

	contractDeployGas = 5000000
)

// fields required for RequestMpc call
type MpcRequest struct {
	NumWorkers                *big.Int
	ProgramHash               string
	DataProviderWorkers       []common.Address
	DelegatedWorker           common.Address
	FeeForDataProviderWorkers []*big.Int
	FeePerWorker              *big.Int
	TotalFee                  *big.Int
}

var programHash = "Test"
var mpcRequest *MpcRequest
var accounts []model.Account
var workerList []common.Address

func init() {

	goPath := os.Getenv("GOPATH")
	keyFile := goPath + data.RelativePath
	var err error
	accounts, err = LoadAccounts(keyFile)
	if err != nil {
		log.Panic(err)
	}

	mpcRequest = &MpcRequest{
		NumWorkers:                big.NewInt(4),
		ProgramHash:               programHash,
		DataProviderWorkers:       []common.Address{accounts[DataProviderWorker].Address},
		DelegatedWorker:           accounts[DelegatedWorker].Address,
		FeeForDataProviderWorkers: []*big.Int{new(big.Int).Exp(big.NewInt(10), big.NewInt(18), nil)},
		FeePerWorker:              new(big.Int).Exp(big.NewInt(10), big.NewInt(18), nil),
	}

	workerList = []common.Address{
		accounts[DataProviderWorker].Address,
		accounts[Worker1].Address,
		accounts[Worker2].Address,
		accounts[DelegatedWorker].Address,
	}

}

func LoadAccounts(sourceFile string) ([]model.Account, error) {
	var skStrings []string
	f, err := os.Open(sourceFile)
	defer f.Close()
	if err != nil {
		return nil, err
	}
	jsonParser := json.NewDecoder(f)
	err = jsonParser.Decode(&skStrings)
	if err != nil {
		return nil, err
	}
	accounts := make([]model.Account, 0)
	for _, skString := range skStrings {
		sk, err := crypto.HexToECDSA(skString[2:]) // remove the leading "0x"
		if err != nil {
			return nil, err
		}
		address, err := util.GetAddrFromSk(sk)
		if err != nil {
			return nil, err
		}
		accounts = append(accounts, model.Account{sk, address})
	}
	return accounts, nil
}

func DeployArpaMpc(
	ctx context.Context,
	client *ethclient.Client,
	account model.Account,
	coordinatorAddress common.Address,
) (common.Address, *contract.Contract, error) {
	privateKey := account.PrivateKey

	optsGen := transaction.NewOptsGenerator(privateKey, client)
	opts, err := optsGen.GenerateOpts(ctx, big.NewInt(0), contractDeployGas)
	if err != nil {
		return common.HexToAddress("0x0"), nil, err
	}

	address, _, instance, err := contract.DeployContract(opts, client, coordinatorAddress)
	if err != nil {
		return common.HexToAddress("0x0"), nil, err
	}

	return address, instance, nil
}

// get an contract instance at a given address
func LoadArpaMpc(client *ethclient.Client, contractAddress common.Address) (*contract.Contract, error) {

	instance, err := contract.NewContract(contractAddress, client)
	if err != nil {
		return nil, err
	}
	return instance, nil

}

func UpdateWorkerList(
	ctx context.Context,
	client *ethclient.Client,
	account model.Account,
	instance *contract.Contract,
	workersToAdd, workersToRemove []common.Address,
) (*types.Transaction, error) {
	privateKey := account.PrivateKey

	optsGen := transaction.NewOptsGenerator(privateKey, client)
	opts, err := optsGen.GenerateDefaultOpts(ctx, big.NewInt(0))
	if err != nil {
		return nil, err
	}
	tx, err := instance.UpdateWorkerList(
		opts,
		workersToAdd,
		workersToRemove,
	)
	if err != nil {
		return nil, err
	}

	return tx, nil
}

func SendMpcRequest(ctx context.Context, client *ethclient.Client, account model.Account, instance *contract.Contract, mpcRequest *MpcRequest) (*types.Transaction, error) {

	privateKey := account.PrivateKey

	optsGen := transaction.NewOptsGenerator(privateKey, client)
	opts, err := optsGen.GenerateDefaultOpts(ctx, mpcRequest.TotalFee)
	if err != nil {
		return nil, err
	}

	tx, err := instance.RequestMpc(
		opts,
		mpcRequest.NumWorkers,
		mpcRequest.ProgramHash,
		mpcRequest.DataProviderWorkers,
		mpcRequest.DelegatedWorker,
		mpcRequest.FeeForDataProviderWorkers,
		mpcRequest.FeePerWorker,
	)
	if err != nil {
		return nil, err
	}

	return tx, nil
}

func Deploy() {

	client, err := ethclient.Dial("ws://127.0.0.1:8545")
	if err != nil {
		log.Fatal(err)
	}
	coordinator := accounts[Coordinator]

	coordinatorAddress := coordinator.Address

	ctx := context.Background()

	contractAddress, instance, err := DeployArpaMpc(ctx, client, coordinator, coordinatorAddress)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Contract deployed")
	_ = contractAddress

	user := accounts[User]

	_, err = UpdateWorkerList(ctx, client, coordinator, instance, workerList, []common.Address{})
	if err != nil {
		log.Panic(err)
	}
	fmt.Println("Worker list updated")

	//query := ethereum.FilterQuery{
	//	Addresses: []common.Address{contractAddress},
	//}
	//logs := make(chan types.Log)
	//sub, err := client.SubscribeFilterLogs(context.Background(), query, logs)
	//if err != nil {
	//	log.Fatal(err)
	//}

	_, err = SendMpcRequest(ctx, client, user, instance, mpcRequest)
	if err != nil {
		log.Panic(err)
	}
	fmt.Println("Mpc Request Sent")

	//for {
	//	select {
	//	case err := <-sub.Err():
	//		log.Fatal(err)
	//	case vLog := <-logs:
	//		fmt.Printf("%x\n", vLog.Data) // pointer to event log
	//	}
	//}

}
