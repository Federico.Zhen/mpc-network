package fake

import (
	"context"
	"log"
	"math/big"
	"sync"

	"gitlab.com/arpachain/mpc/eth/pkg/transaction"
)

type fakeEmptyDispatcher struct {
	mu sync.Mutex
}

func NewFakeEmptyDispatcher() transaction.Dispatcher {
	return &fakeEmptyDispatcher{}
}

// Only fire callContract once, do nothing else
func (d *fakeEmptyDispatcher) Send(ctx context.Context, value *big.Int, callContract transaction.CallContract) error {
	d.mu.Lock()
	callContract(nil) // optsGen is mock one in test
	d.mu.Unlock()
	return nil
}

type fakeNoConfirmationDispatcher struct {
	mu            sync.Mutex
	optsGenerator transaction.OptsGenerator
}

func NewFakeNoConfirmationDispatcher(optsGenerator transaction.OptsGenerator) transaction.Dispatcher {
	return &fakeNoConfirmationDispatcher{
		optsGenerator: optsGenerator,
	}
}

// Only send the tx, do not confirm
func (d *fakeNoConfirmationDispatcher) Send(ctx context.Context, value *big.Int, callContract transaction.CallContract) error {
	d.mu.Lock()
	opts, err := d.optsGenerator.GenerateDefaultOpts(ctx, value)
	if err != nil {
		log.Fatal("Fake dispatcher failed to generate tx opts!")
	}
	callContract(opts) // optsGen is mock one in test
	d.mu.Unlock()
	return nil
}
