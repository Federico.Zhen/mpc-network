package handler

import (
	"context"
	"log"
	"math/big"
	"time"

	pb "gitlab.com/arpachain/mpc/eth/api"
	"gitlab.com/arpachain/mpc/eth/pkg/abstract"
	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/pkg/parser"
	"gitlab.com/arpachain/mpc/eth/pkg/request"
	"gitlab.com/arpachain/mpc/eth/pkg/transaction"
	"gitlab.com/arpachain/mpc/eth/pkg/util"
	"gitlab.com/arpachain/mpc/eth/pkg/worker_list"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
)

const (
	timeout    = time.Hour // timeout for ethereum related transation
	numRetries = 3
)

var retryIntervals = []time.Duration{
	1 * time.Second,
	3 * time.Second,
	10 * time.Second,
	0 * time.Second, // no need to sleep since the final retry has failed
}

// Handler for event received by the listener
type Handler interface {
	HandleRequestReceivedEvent(event *model.Event)
	HandleComputationStartedEvent(event *model.Event)
	HandleResultPostedEvent(event *model.Event)
	HandleWorkerListUpdatedEvent(event *model.Event)
	HandleReportMpcResultBlockchain(req *pb.ReportMpcResultRequest)
	HandleUpdateWorkerListBlockchain(req *pb.UpdateWorkerListRequest)
}

// Reference implementation:
type handler struct {
	client          pb.ArpaMpcClient
	parser          parser.Parser
	optsGenerator   transaction.OptsGenerator
	instance        abstract.Contract
	startedChanMap  map[int64]chan bool
	finishedChanMap map[int64]chan bool
	reqRepo         request.Repository
	workerRepo      worker_list.Repository
	dispatcher      transaction.Dispatcher
}

// NewHandler returns an instance of eth.Handler
func NewHandler(
	client pb.ArpaMpcClient,
	parser parser.Parser,
	instance abstract.Contract,
	requestRepository request.Repository,
	workerListRepository worker_list.Repository,
	dispatcher transaction.Dispatcher,
) Handler {
	startedChanMap := make(map[int64]chan bool)
	finishedChanMap := make(map[int64]chan bool)
	return &handler{
		client:          client,
		parser:          parser,
		instance:        instance,
		startedChanMap:  startedChanMap,
		finishedChanMap: finishedChanMap,
		reqRepo:         requestRepository,
		workerRepo:      workerListRepository,
		dispatcher:      dispatcher,
	}
}

func (h *handler) HandleRequestReceivedEvent(event *model.Event) {
	log.Printf("Eth handler: handling RequestReceivedEvent for tx: %v", event.Log.TxHash.Hex())
	logRequestReceived, err := h.parser.ParseRequestReceivedEvent(event)
	if err != nil {
		log.Println("Eth handler: Failed to parse a RequestReceived event")
		return
	}
	reqID := logRequestReceived.ReqId.Int64()
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	dataProviderWorkers := make([]string, len(logRequestReceived.DataProviderWorkers))
	for index, worker := range logRequestReceived.DataProviderWorkers {
		dataProviderWorkers[index] = worker.Hex()
	}
	var delegatedWorker string
	if logRequestReceived.DelegatedWorker == common.HexToAddress("0x0") {
		delegatedWorker = ""
	} else {
		delegatedWorker = logRequestReceived.DelegatedWorker.Hex()
	}
	request := &pb.RunMpcForBlockchainRequest{
		ProgramHash:                    logRequestReceived.ProgramHash,
		NumTotalWorkers:                uint32(logRequestReceived.NumWorkers.Uint64()),
		DataProviderWorkerBlockchainAddresses: dataProviderWorkers,
		DelegatedWorkerBlockchainAddress:      delegatedWorker,
	}
	log.Printf("Eth handler: calling coordinator.RunMpcForBlockchain(...) for request: %d\n", reqID)

	var res *pb.RunMpcForBlockchainResponse
	for _, interval := range retryIntervals {
		res, err = h.client.RunMpcForBlockchain(ctx, request)
		if err == nil {
			break
		}
		time.Sleep(interval)
	}
	if err != nil {
		log.Printf("Eth handler: RunMpcForBlockchain failed, request ID: %d: %v\n", reqID, err)
		return
	}
	log.Printf("Eth handler: called coordinator.RunMpcForBlockchain(...), got response: %v\n", res)

	err = h.reqRepo.Insert(res.MpcId, logRequestReceived.ReqId.Int64())
	if err != nil {
		log.Printf("Eth handler: failed to persist reqID for request %d\n", reqID)
		return
	}

	// send computationStarted
	startedChan := make(chan bool)
	h.startedChanMap[reqID] = startedChan
	defer close(startedChan)

	workerList := make([]common.Address, len(res.AdditionalWorkerBlockchainAddresses))
	for index, worker := range res.AdditionalWorkerBlockchainAddresses {
		workerList[index] = common.HexToAddress(worker)
	}

	callStartComputation := func(opts *bind.TransactOpts) (*types.Transaction, error) {
		tx, err := h.instance.StartComputation(opts, big.NewInt(reqID), workerList)
		log.Println("Start computation sent")
		return tx, err
	}

	errChan := make(chan error)
	defer close(errChan)
	for i := 0; i < numRetries; i++ {
		go func() {
			errChan <- h.dispatcher.Send(ctx, big.NewInt(0), callStartComputation)
		}()

		// This is to consume the event in startedChan filled in by
		// Router right after receiving the event.
		// The logic itself are not necessary and need to be refactor in the future
		select {
		case <-startedChan:
			log.Printf("Eth handler: StartComputation posted for request %d\n", reqID)
		case <-time.After(timeout):
			log.Printf("Eth handler: cannot confirm StartComputation posted for request %d\n", reqID)
			continue
		}

		// The following logic avoids we proceed before dispatcher.Send() finishes.
		// Otherwise the dispatcher.Send() may be terminated early as the ctx
		// is canceled right after the calling function returns.
		select {
		case err := <-errChan:
			if err != nil {
				log.Printf("Eth handler: error while sending StartComputation: %v", err)
				continue
			}
			return
		case <-time.After(timeout):
			log.Printf("Eth handler: cannot confirm StartComputation posted for request %d\n", reqID)
			continue
		}
	}

}

func (h *handler) HandleComputationStartedEvent(event *model.Event) {
	log.Printf("Eth handler: handling ComputationStartedEvent for tx: %v", event.Log.TxHash.Hex())
	logComputationStarted, err := h.parser.ParseComputationStartedEvent(event)
	if err != nil {
		log.Println("Eth handler: failed to parse a ComputationStarted event")
		return
	}
	reqID := logComputationStarted.ReqId.Int64()
	log.Printf("Eth handler: ComputationStartedHandled for req: %d", reqID)
	h.startedChanMap[reqID] <- true
}

func (h *handler) HandleResultPostedEvent(event *model.Event) {
	log.Printf("Eth handler: handling ResultPostedEvent for tx: %v", event.Log.TxHash.Hex())
	logResultPosted, err := h.parser.ParseResultPostedEvent(event)
	if err != nil {
		log.Println("Eth handler: failed to parse a ResultPosted event")
		return
	}
	reqID := logResultPosted.ReqId.Int64()
	log.Printf("Eth handler: ResultPostedEventHandled for req: %d", reqID)
	h.finishedChanMap[reqID] <- true
}

func (h *handler) HandleWorkerListUpdatedEvent(event *model.Event) {
	log.Printf("Eth handler: handling WorkerListUpdatedEvent for tx: %v", event.Log.TxHash.Hex())
	logWorkerListUpdated, err := h.parser.ParseWorkerListUpdatedEvent(event)
	if err != nil {
		log.Println("Eth handler: failed to parse a WorkerListUpdated event")
		return
	}
	log.Println("Eth handler: Storing worker list...")
	err = h.workerRepo.StoreWorkerList(logWorkerListUpdated.WorkerList)
	if err != nil {
		log.Printf("Eth handler: err while persist worker list: %v\n", err)
		return
	}
}

func (h *handler) HandleReportMpcResultBlockchain(req *pb.ReportMpcResultRequest) {
	mpcID := req.MpcId
	var reqID int64
	var exists bool
	var err error
	reqID, exists, err = h.reqRepo.LookUp(mpcID)
	if err != nil {
		log.Printf("Eth handler: error while finding the corresponding requestId for the given mpcID: %v\n", err)
		return
	}
	if !exists {
		log.Println("Eth handler: failed to find the corresponding requestId for the given mpcID")
	}
	log.Printf("Eth handler: handling report MPC result for req: %d\n", reqID)
	// TODO(Jiang): This is a temporary hack. Currently worker may return
	// different result as the worker 0 returns extra debug string.
	// Usually there is only one worker that returns result, and the other works
	// returns nothing. So we pick the one with longest result.
	// In the future, We need to better handle what result to post.
	longestResult := ""
	maxLength := 0
	for _, result := range req.Result {
		if len(result.Data) > maxLength {
			longestResult = result.Data
			maxLength = len(result.Data)
		}
	}
	log.Printf("Eth handler: Selected MPC result:\n%v\n", longestResult)

	finishedChan := make(chan bool)
	h.finishedChanMap[reqID] = finishedChan
	defer close(finishedChan)

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	callPostResult := func(opts *bind.TransactOpts) (*types.Transaction, error) {
		tx, err := h.instance.PostResult(opts, big.NewInt(reqID), longestResult)
		return tx, err
	}

	errChan := make(chan error)
	defer close(errChan)
	for i := 0; i < numRetries; i++ {
		go func() {
			errChan <- h.dispatcher.Send(ctx, big.NewInt(0), callPostResult)
		}()

		// This is to consume the event in finishedChan filled in by
		// Router right after receiving the event.
		// The logic itself are not necessary and need to be refactor in the future
		select {
		case <-finishedChan:
			log.Printf("Eth handler: PostResult posted for request %d\n", reqID)
			log.Printf("Eth handler: Request %d done!\n", reqID)
		case <-time.After(timeout):
			log.Printf("Eth handler: cannot confirm PostResult posted for request %d\n", reqID)
			continue
		}

		// The following logic avoids we proceed before dispatcher.Send() finishes.
		// Otherwise the dispatcher.Send() may be terminated early as the ctx
		// is canceled right after the calling function returns.
		select {
		case err := <-errChan:
			if err != nil {
				log.Printf("Eth handler: error while sending PostResult: %v", err)
				continue
			}
			return
		case <-time.After(timeout):
			log.Printf("Eth handler: cannot confirm PostResult posted for request %d\n", reqID)
			continue
		}
	}
}

func (h *handler) HandleUpdateWorkerListBlockchain(req *pb.UpdateWorkerListRequest) {
	newWorkerList := make([]common.Address, len(req.WorkerAddresses))
	for index, worker := range req.WorkerAddresses {
		newWorkerList[index] = common.HexToAddress(worker)
	}
	oldWorkerList, exists, err := h.workerRepo.GetWorkerList()
	if err != nil {
		log.Printf("Eth handler:  err while get worker list: %v", err)
		return
	}
	if !exists {
		oldWorkerList = []common.Address{}
	}
	workersToAdd := util.Difference(newWorkerList, oldWorkerList)
	workersToRemove := util.Difference(oldWorkerList, newWorkerList)
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	callUpdateWorkerList := func(opts *bind.TransactOpts) (*types.Transaction, error) {
		tx, err := h.instance.UpdateWorkerList(opts, workersToAdd, workersToRemove)
		return tx, err
	}
	err = h.dispatcher.Send(ctx, big.NewInt(0), callUpdateWorkerList)
	if err != nil {
		log.Printf("Eth handler:  failed to send UpdateWorkerList tx: %v", err)
		return
	}

	// TODO(John): may use a chan with HandleWorkerListUpdatedEvent
	// to confirm the result
}
