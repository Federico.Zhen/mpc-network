package request

import (
	"encoding/json"

	db_utils "gitlab.com/arpachain/mpc/persistent/pkg/db"
	"github.com/dgraph-io/badger"
)

const (
	KeyPrefix = "ethreq_"
)

type Repository interface {
	Insert(mpcID string, reqID int64) error
	LookUp(mpcID string) (int64, bool, error)
}

// a BadgerDB-based implementation
type badgerRepository struct {
	db *badger.DB
}

// NewBadgerRepository returns a BadgerDB-based implementation
func NewBadgerRepository(db *badger.DB) Repository {
	return &badgerRepository{
		db: db,
	}
}

func (r *badgerRepository) Insert(mpcID string, reqID int64) error {
	reqIDBytes, err := json.Marshal(reqID)
	if err != nil {
		return err
	}
	err = db_utils.CreateObject(r.db, KeyPrefix, mpcID, reqIDBytes)
	if err != nil {
		return err
	}
	return nil
}

// Look up a mpcID and return the corresponding reqID(int64).
// If the mpcID is not found in the db, the second argument returned will be false, without an error.
// Therefore, first check the error, then check if the value is found, finally use the value.
func (r *badgerRepository) LookUp(mpcID string) (int64, bool, error) {
	var reqID int64
	exists, err := db_utils.GetObject(r.db, KeyPrefix, mpcID, &reqID)
	if err != nil {
		return reqID, false, err
	}
	if !exists {
		return reqID, false, nil
	}
	return reqID, true, nil
}
