package transaction

import (
	"context"
	"errors"
	"log"
	"math/big"
	"sync"
	"time"

	"gitlab.com/arpachain/mpc/eth/pkg/abstract"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
)

const waitingTime = time.Hour

var retryIntervals = []time.Duration{
	1 * time.Second,
	5 * time.Second,
	20 * time.Second,
	50 * time.Second,
	100 * time.Second,
	0 * time.Second, // no need to sleep since the final retry has failed
}

type CallContract func(opts *bind.TransactOpts) (*types.Transaction, error)

type Dispatcher interface {
	Send(ctx context.Context, value *big.Int /* amount of wei to send */, callContract CallContract) error
}

type dispatcher struct {
	client                                                   abstract.ClientForTxConfirmation
	optsGenerator                                            OptsGenerator
	mu                                                       sync.Mutex
	numConfirmations                                         int64
	pollingIntervalForMining, pollingIntervalForConfirmation time.Duration
}

func NewDispatcher(
	client abstract.ClientForTxConfirmation,
	optsGenerator OptsGenerator,
	numConfirmations int64,
	pollingIntervalForMining, pollingIntervalForConfirmation time.Duration,
) Dispatcher {
	return &dispatcher{
		client:                         client,
		optsGenerator:                  optsGenerator,
		numConfirmations:               numConfirmations,
		pollingIntervalForMining:       pollingIntervalForMining,
		pollingIntervalForConfirmation: pollingIntervalForConfirmation,
	}
}

// This function grabs the lock on Dispatcher and calls callContract to send an transaction,
// then unlock Dispatcher and wait for a number of confirmations and then return.
func (d *dispatcher) Send(ctx context.Context, value *big.Int, callContract CallContract) error {

	d.mu.Lock()

	var opts *bind.TransactOpts
	var err error
	for _, interval := range retryIntervals {
		opts, err = d.optsGenerator.GenerateDefaultOpts(ctx, value)
		if err == nil {
			break
		}
		time.Sleep(interval)
	}
	if err != nil {
		log.Printf("Eth dispatcher: failed to get transaction options: %v", err)
		d.mu.Unlock()
		return err
	}

	var tx *types.Transaction
	for _, interval := range retryIntervals {
		tx, err = callContract(opts)
		if err == nil {
			break
		}
		time.Sleep(interval)
	}
	if err != nil {
		log.Printf("Eth dispatcher: failed to call contract: %v", err)
		d.mu.Unlock()
		return err
	}
	log.Printf("Eth dispatcher: tx %v sent\n", tx.Hash().Hex())

	minedBlockNum, err := checkMined(ctx, d.client, d.pollingIntervalForMining, tx.Hash())
	d.mu.Unlock()
	if err != nil {
		log.Printf("Warning: Eth dispatcher: Tx %v failed to get mined\n", tx.Hash().Hex())
		return err
	}

	err = checkConfirmation(
		ctx,
		d.client,
		d.pollingIntervalForConfirmation,
		minedBlockNum,
		d.numConfirmations,
		tx.Hash(),
	)
	if err != nil {
		log.Printf("Warning: Eth dispatcher: failed to confirm tx %v: %v\n", tx.Hash().Hex(), err)
		return err
	}
	log.Printf("Eth dispatcher: tx %v confirmed\n", tx.Hash().Hex())
	return nil
}

func checkMined(ctx context.Context,
	client abstract.ClientForTxConfirmation,
	pollingIntervalForMining time.Duration,
	txHash common.Hash,
) (*big.Int, error) {

	numRetries := int(waitingTime / pollingIntervalForMining)
	if numRetries == 0 {
		numRetries = 1 // at least try once
	}

	for i := 0; i < numRetries; i++ {
		time.Sleep(pollingIntervalForMining)

		receipt, err := client.TransactionReceipt(ctx, txHash)
		if err != nil {
			continue // not mined yet
		}
		if receipt.Status != types.ReceiptStatusSuccessful {
			return nil, err
		}

		var header *types.Header
		for _, interval := range retryIntervals {

			header, err = client.HeaderByNumber(ctx, nil)
			if err == nil {
				return header.Number, nil
			}
			time.Sleep(interval)
		}

		return nil, err // failed to get header

	}

	return nil, errors.New("Timeout when confirming tx mining")

}

// check if the tx received enough confirmations
func checkConfirmation(ctx context.Context,
	client abstract.ClientForTxConfirmation,
	pollingIntervalForConfirmation time.Duration,
	minedBlockNum *big.Int,
	numConfirmations int64,
	txHash common.Hash,
) error {
	for {
		time.Sleep(pollingIntervalForConfirmation)

		var currentHeader *types.Header
		var err error
		for _, interval := range retryIntervals {
			currentHeader, err = client.HeaderByNumber(ctx, nil)
			if err == nil {
				break
			}
			time.Sleep(interval)
		}
		if err != nil {
			return err
		}

		currentBlockNum := currentHeader.Number
		height := big.NewInt(0).Sub(currentBlockNum, minedBlockNum).Int64()
		if height < numConfirmations {
			continue
		}

		// to confirm that the tx is not rolled back
		for _, interval := range retryIntervals {
			_, err = client.TransactionReceipt(ctx, txHash)
			if err == nil {
				break
			}
			time.Sleep(interval)
		}
		return err
	}
}
