package model

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
)

// go-version struct for events
type LogRequestReceived struct {
	ReqId                     *big.Int
	NumWorkers                *big.Int
	ProgramHash               string
	DataConsumer              common.Address
	DataProviderWorkers       []common.Address
	DelegatedWorker           common.Address
	FeeForDataProviderWorkers []*big.Int
	FeePerAdditionalWorker    *big.Int
}

type LogComputationStarted struct {
	ReqId *big.Int
}

type LogResultPosted struct {
	ReqId  *big.Int
	Result string
}

type LogWorkerListUpdated struct {
	WorkerList []common.Address
}

// Event signature string
const (
	logRequestReceivedSig    = "RequestReceived(uint256,uint256,string,address,address[],address,uint256[],uint256)"
	logComputationStartedSig = "ComputationStarted(uint256)"
	logResultPostedSig       = "ResultPosted(uint256,string)"
	logWorkerListUpdatedSig  = "WorkerListUpdated(address[])"
)

// LogSigHashMap stores the event signature that needed by router
type LogSigHashMap interface {
	GetRequestReceivedSigHash() common.Hash
	GetComputationStartedSigHash() common.Hash
	GetResultPostedSigHash() common.Hash
	GetWorkerListUpdatedSigHash() common.Hash
}

// Reference implementation:
type logSigHashMap struct {
	hashMap map[string]common.Hash
}

// NewLogSigHashMap returns an instance of eth.LogSigHashMap
func NewLogSigHashMap() LogSigHashMap {
	hashMap := map[string]common.Hash{
		"RequestReceived":    crypto.Keccak256Hash([]byte(logRequestReceivedSig)),
		"ComputationStarted": crypto.Keccak256Hash([]byte(logComputationStartedSig)),
		"ResultPosted":       crypto.Keccak256Hash([]byte(logResultPostedSig)),
		"WorkerListUpdated":  crypto.Keccak256Hash([]byte(logWorkerListUpdatedSig)),
	}
	return &logSigHashMap{hashMap}
}

func (l *logSigHashMap) GetRequestReceivedSigHash() common.Hash {
	return l.hashMap["RequestReceived"]
}

func (l *logSigHashMap) GetComputationStartedSigHash() common.Hash {
	return l.hashMap["ComputationStarted"]
}

func (l *logSigHashMap) GetResultPostedSigHash() common.Hash {
	return l.hashMap["ResultPosted"]
}

func (l *logSigHashMap) GetWorkerListUpdatedSigHash() common.Hash {
	return l.hashMap["WorkerListUpdated"]
}
