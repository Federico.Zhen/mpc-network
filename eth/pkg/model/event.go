package model

import (
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
)

// EventType is an enum used to identify the type of an event
type EventType int

const (
	// RequestReceived is the type of event
	// emitted when a mpc is requested on eth
	// the enum value is 0
	RequestReceived EventType = iota
	// ComputationStarted is the type of event
	// emitted when a mpc is started by coordinator
	// the enum value is 1
	ComputationStarted
	// WorkerListUpdated is the type of event
	// emitted when a coordinator updates the list of workers
	// the enum value is 2
	WorkerListUpdated
	// ResultPosted is the type of event
	// emitted when a coordinator posts the result of a mpc
	// the enum value is 3
	ResultPosted
)

// Event is the model of an event of smart contract
type Event struct {
	Name            string
	Type            EventType
	ContractAddress common.Address
	Timestamp       int64
	Log             types.Log
}
