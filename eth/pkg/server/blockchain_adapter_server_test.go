package server_test

import (
	"context"
	"testing"
	"time"

	pb "gitlab.com/arpachain/mpc/eth/api"
	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/pkg/server"
	"gitlab.com/arpachain/mpc/eth/test/fake"
	"github.com/stretchr/testify/assert"
)

var testWorkers = [2][]string{
	[]string{
		"Test Worker 1",
		"Test Worker 2",
		"Test Worker 3",
	},
	[]string{
		"Test Worker 1",
		"Test Worker 2",
		"Test Worker 3",
		"Test Worker 4",
		"Test Worker 5",
		"Test Worker 6",
	},
}

func TestUpdateWorkerList(t *testing.T) {
	assert := assert.New(t)

	eventChan := make(chan int)
	logSigHashMap := model.NewLogSigHashMap()
	fakeHandler := fake.NewFakeHandler(eventChan, assert, logSigHashMap)
	server := server.NewBlochchainAdapterServer(fakeHandler)

	t.Log("Sending the 1st update worker list request")
	ctx := context.Background()
	req := &pb.UpdateWorkerListRequest{
		WorkerAddresses: testWorkers[0],
	}
	res, err := server.UpdateWorkerList(ctx, req)
	assert.NoError(err)
	assert.Equal(true, res.Success)

	select {
	case eventName := <-eventChan:
		assert.Equal(fake.UpdateWorkerListBlockchainHandled, eventName)
	case <-time.After(time.Millisecond):
		t.Fatalf("No response from updateWorkerListBlockchain after a reasonable amount of time\n")
	}

	t.Log("Sending a duplicated update worker list request")
	res, err = server.UpdateWorkerList(ctx, req)
	assert.NoError(err)
	assert.Equal(true, res.Success)

	select {
	case <-eventChan:
		t.Fatal("Duplicated update worker list request got handled")
	case <-time.After(time.Millisecond):
		t.Log("Duplicated update worker list request is dropped")
	}

	t.Log("Sending the 2nd update worker list request")
	req = &pb.UpdateWorkerListRequest{
		WorkerAddresses: testWorkers[1],
	}
	res, err = server.UpdateWorkerList(ctx, req)
	assert.NoError(err)
	assert.Equal(true, res.Success)

	select {
	case eventName := <-eventChan:
		assert.Equal(fake.UpdateWorkerListBlockchainHandled, eventName)
	case <-time.After(time.Millisecond):
		t.Fatalf("No response from updateWorkerListBlockchain after a reasonable amount of time\n")
	}

}
