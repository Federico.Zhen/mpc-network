package parser

import (
	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"github.com/ethereum/go-ethereum/accounts/abi"
)

// Parser parses an event emitted from Ethereum Smart Contract
// and converts it into a RunMpcRequest
type Parser interface {
	ParseRequestReceivedEvent(event *model.Event) (*model.LogRequestReceived, error)
	ParseComputationStartedEvent(event *model.Event) (*model.LogComputationStarted, error)
	ParseResultPostedEvent(event *model.Event) (*model.LogResultPosted, error)
	ParseWorkerListUpdatedEvent(event *model.Event) (*model.LogWorkerListUpdated, error)
}

// Reference implementation
type parser struct {
	contractAbi abi.ABI
}

// NewParser returns an instance of eth.Parser
func NewParser(contractAbi abi.ABI) Parser {
	return &parser{
		contractAbi: contractAbi,
	}
}

func (p *parser) ParseRequestReceivedEvent(event *model.Event) (*model.LogRequestReceived, error) {

	var logRequestReceived model.LogRequestReceived
	err := p.contractAbi.Unpack(&logRequestReceived, "RequestReceived", event.Log.Data)
	if err != nil {
		return nil, err
	}
	return &logRequestReceived, nil
}

func (p *parser) ParseComputationStartedEvent(event *model.Event) (*model.LogComputationStarted, error) {

	var logComputationStarted model.LogComputationStarted

	err := p.contractAbi.Unpack(&logComputationStarted, "ComputationStarted", event.Log.Data)
	if err != nil {
		return nil, err
	}

	return &logComputationStarted, nil
}

func (p *parser) ParseResultPostedEvent(event *model.Event) (*model.LogResultPosted, error) {

	var logResultPosted model.LogResultPosted
	err := p.contractAbi.Unpack(&logResultPosted, "ResultPosted", event.Log.Data)
	if err != nil {
		return nil, err
	}

	return &logResultPosted, nil
}

func (p *parser) ParseWorkerListUpdatedEvent(event *model.Event) (*model.LogWorkerListUpdated, error) {

	var logWorkerListUpdated model.LogWorkerListUpdated

	err := p.contractAbi.Unpack(&logWorkerListUpdated, "WorkerListUpdated", event.Log.Data)
	if err != nil {
		return nil, err
	}

	return &logWorkerListUpdated, nil
}
