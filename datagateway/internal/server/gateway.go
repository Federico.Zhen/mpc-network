package server

import (
	"context"
	"fmt"

	pb "gitlab.com/arpachain/mpc/datagateway/api"
	"gitlab.com/arpachain/mpc/datagateway/internal/data"
)

type dataGatewayServer struct {
	repository data.Repository
	// currently, the proto api is not specifying which test data set is needed from worker
	// I suspect this would change so I leave it open for extension
	// mpcDataName is used to distinguish which set of data to use
	dataSetName string
}

// NewDataGatewayServer returns an instance of
// the implementation of the DataGateway service defined in proto
func NewDataGatewayServer(repository data.Repository, dataSetName string) pb.DataGatewayServer {
	return &dataGatewayServer{
		dataSetName: dataSetName,
		repository:  repository,
	}
}

func (s *dataGatewayServer) GetData(
	ctx context.Context,
	in *pb.GetDataRequest,
) (*pb.GetDataResponse, error) {
	mpcData, err := s.repository.GetByName(s.dataSetName)
	if err != nil {
		return nil, err
	}
	result, exists := mpcData.RawData[in.Key]
	if !exists {
		return nil, fmt.Errorf("provided key: %v doesn't exist, please check whether you are using the right MPC data name", in.Key)
	}
	response := &pb.GetDataResponse{
		Result: result,
	}
	return response, nil
}

func (s *dataGatewayServer) GetAllData(
	ctx context.Context,
	in *pb.GetAllDataRequest,
) (*pb.GetAllDataResponse, error) {
	mpcData, err := s.repository.GetByName(s.dataSetName)
	if err != nil {
		return nil, err
	}
	response := &pb.GetAllDataResponse{
		Result: mpcData.RawData,
	}
	return response, nil
}
