package data

// Data as a single MPC data object
type Data struct {
	Name    string
	RawData map[int64]int64
}
