#!/bin/bash

set -ex
cd $GOPATH/src/gitlab.com/arpachain/mpc/datagateway/
go test ./... -cover
